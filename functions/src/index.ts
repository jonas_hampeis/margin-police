import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { CalculatePrice } from '../../src/app/models/calculatePrice.model';
import { Comparison } from '../../src/app/models/comparison.model';
import { Timestamp } from '@google-cloud/firestore';
import { Tariff } from '../../src/app/models/tariff.model';
import { Provider } from '../../src/app/models/provider.model';

admin.initializeApp();

exports.deleteEntries = functions.https.onCall(async (data, context) => {
  if (data.userId !== context.auth?.uid) {
    console.log('user doesnt match with context');
    return 0;
  }
  const entries = await admin
    .firestore()
    .collection(
      `users/${data.userId}/supplyPoint/${data.supplyPointUid}/tariff/${data.tariffUid}/entries`
    )
    .listDocuments();
  for (const entry of entries) {
    entry.delete().catch((err) => console.log(err));
  }
  return 1;
});

exports.calculatePrice = functions.https.onCall(
  async (data: CalculatePrice) => {
    const comparisons = new Array<Comparison>();

    console.log({ ProvidedData: data });

    // some input data is optional
    // this block loads default data for those optional values
    if (!data.rateId) {
      data.rateId = +(
        await admin
          .firestore()
          .collection('rates')
          .where('default', '==', true)
          .get()
      ).docs[0].id;
    }
    if (!data.circuitBreakerId) {
      data.circuitBreakerId = +(
        await admin
          .firestore()
          .collection('circuitBreakers')
          .where('default', '==', true)
          .get()
      ).docs[0].id;
    }
    if (!data.provider) {
      data.provider = (
        await admin
          .firestore()
          .collection('providers')
          .where('default', '==', true)
          .get()
      ).docs[0].id;
    }
    if (!data.tariff || data.tariff === 'Unknown Tariff') {
      data.tariff = (
        await admin
          .firestore()
          .collection(`providers/${data.provider}/tariffs`)
          .where('default', '==', true)
          .get()
      ).docs[0].id;
    }
    if (!data.distributor) {
      data.distributor = (
        await admin
          .firestore()
          .collection('distributors')
          .where('default', '==', true)
          .get()
      ).docs[0].id;
    }
    if (!data.period) {
      data.period = 365;
    }
    // get months from period
    const periodMonths = data.period / 30.41;
    // console.log({ period: data.period, months: periodMonths });

    // We dont have any other data than for these two rates
    // MUST BE DELETED WHEN WE HAVE ALL DATA
    if (data.rateId !== 1 && data.rateId !== 9) {
      data.rateId = 1;
    }

    console.log({ DataAfterDefaultsSet: data });

    // get DISTRIBUTOR price list
    // should return lists for same rates
    const distributorPriceLists = await admin
      .firestore()
      .collection(`distributors/${data.distributor}/priceLists`)
      .where('rateId', '==', data.rateId)
      .get();

    // filter pricelists for current time period
    // hopefully after this we have only one pricelist
    // needs error handling
    const distributorPriceList = [];
    for (const i of distributorPriceLists.docs) {
      if (
        i.data().validTo.seconds >= Timestamp.now().seconds &&
        i.data().validFrom.seconds <= Timestamp.now().seconds
      ) {
        distributorPriceList.push(i.data());
      }
    }

    // same thing as before, but for REGULATED SERVICES
    const regulatedServicesPriceLists = await admin
      .firestore()
      .collection(`regulatedServices`)
      .where('validFrom', '<=', Timestamp.now())
      .get();

    const regulatedServicesPriceList = [];
    for (const i of regulatedServicesPriceLists.docs) {
      if (i.data().validTo.seconds >= Timestamp.now().seconds) {
        regulatedServicesPriceList.push(i.data());
      }
    }

    // one more time, PROVIDER PRICE LIST
    // if we are getting just one tariff, get that
    // if we are comparing all tariffs, get all of them
    let providerPricelists: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>;
    let currentProviderPricelists: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>;

    currentProviderPricelists = await admin
      .firestore()
      .collection(
        `providers/${data.provider}/tariffs/${data.tariff}/tariffPriceLists`
      )
      .where('rateId', '==', data.rateId)
      .get();

    // console.log({ currentProviderPricelists });

    const currentProviderData = [];
    for (const priceList of currentProviderPricelists.docs) {
      if (
        priceList.data().validTo.seconds >= Timestamp.now().seconds &&
        priceList.data().validFrom.seconds <= Timestamp.now().seconds
      ) {
        // if we are not comparing, get just the one price list
        // if we are comparing, get also tariff and provider info
        // for use in tariff-compare screen
        if (!data.isTariffComparison) {
          currentProviderData.push({ priceList: priceList.data() });
        } else {
          const tariff = (await priceList.ref.parent.parent?.get())?.data();
          const provider = (
            await priceList.ref.parent.parent?.parent.parent?.get()
          )?.data();
          currentProviderData.push({
            priceList: priceList.data(),
            tariff: tariff,
            provider: provider,
          });
        }
      }
    }

    const providerData = [];
    if (data.isTariffComparison) {
      // collectionGroup is such a great function
      // get all tariffPricelists from the whole db
      providerPricelists = await admin
        .firestore()
        .collectionGroup('tariffPriceLists')
        .where('rateId', '==', data.rateId)
        .get();

      for (const priceList of providerPricelists.docs) {
        if (
          priceList.data().validTo.seconds >= Timestamp.now().seconds &&
          priceList.data().validFrom.seconds <= Timestamp.now().seconds
        ) {
          // if we are not comparing, get just the one price list
          // if we are comparing, get also tariff and provider info
          // for use in tariff-compare screen
          if (!data.isTariffComparison) {
            providerData.push({ priceList: priceList.data() });
          } else {
            const tariff = (await priceList.ref.parent.parent?.get())?.data();
            const provider = (
              await priceList.ref.parent.parent?.parent.parent?.get()
            )?.data();
            providerData.push({
              priceList: priceList.data(),
              tariff: tariff,
              provider: provider,
            });
          }
        }
      }
      console.log(providerData);
      if (!providerData) {
        return 'Insufficient provider data. Either wrong input was provided or the data doesn\'t exist.';
      }
    }

    console.log({ currentProviderData });
    console.log({ distributorPriceList });

    if (
      distributorPriceList.length === 0 ||
      regulatedServicesPriceList.length === 0 ||
      currentProviderData.length === 0
    ) {
      return 'Insufficient price list data. Either wrong input was provided or the data doesn\'t exist.';
    }

    // get circuit breaker info if not provided
    const circuitBreaker = await admin
      .firestore()
      .doc(`circuitBreakers/${data.circuitBreakerId}`)
      .get();
    if (!data.phases) {
      data.phases = circuitBreaker.data()?.phase;
    }
    if (!data.circuitBreaker) {
      data.circuitBreaker = circuitBreaker.data()?.circuitBreakerTo;
    }

    //calculation works with MWh, but we use kWh on input
    data.consumption = data.consumption / 1000;

    // if we have tariff with low and high consumption
    // add them together, otherwise use high tariff
    let totalConsumption: number;
    if (data.consumptionLow) {
      data.consumptionLow = data.consumptionLow / 1000;
      totalConsumption = data.consumptionLow + data.consumption;
    } else {
      totalConsumption = data.consumption;
    }

    // distributor's monthly fee also depends on circuit breaker
    // so we get that here from an array of all fees
    const monthlyFeeDistributor = distributorPriceList[0].powerConsumption.find(
      (d: { circuitBreakerId: number; monthlyFee: number }) =>
        d.circuitBreakerId === data.circuitBreakerId
    );

    /*
     *
     *   ACTUAL COST CALCULATIONS
     *
     */

    // create object for distribution prices
    const distributionCost = {
      lowTariff:
        (data.consumptionLow || 0) * (distributorPriceList[0].lowTarif || 0),
      highTariff: data.consumption * (distributorPriceList[0].highTariff || 0),
      powerConsumption: periodMonths * monthlyFeeDistributor.monthlyFee,
    };

    // count total distribution costs
    let totalDistributionCost = 0;
    for (const [, value] of Object.entries(distributionCost)) {
      totalDistributionCost += value;
    }

    // get POZE prices, one depends on circuit breaker
    // the other one on consumption
    // the lower one is used in final price
    let POZEBreaker =
      (data.circuitBreaker || 1) *
      (data.phases || 1) *
      regulatedServicesPriceList[0].POZEBreaker *
      periodMonths;
    let POZEConsumption =
      data.consumption * regulatedServicesPriceList[0].POZEConsumption;

    if (Math.min(POZEBreaker, POZEConsumption) === POZEBreaker) {
      POZEConsumption = 0;
    } else {
      POZEBreaker = 0;
    }

    // create object of regulated services
    // use lower price of the two POZE prices
    const regulatedServicesCost = {
      tax: totalConsumption * regulatedServicesPriceList[0].tax,
      systemServices:
        totalConsumption * regulatedServicesPriceList[0].systemServices,
      OTE: periodMonths * regulatedServicesPriceList[0].OTE,
      POZE: POZEBreaker + POZEConsumption,
    };

    // add up total regulated services cost
    let totalRegulatedServicesCost = 0;
    for (const [, value] of Object.entries(regulatedServicesCost)) {
      totalRegulatedServicesCost += value;
    }

    // create provider cost and add it up
    const providerCost = {
      monthlyFee: periodMonths * currentProviderData[0].priceList.monthlyFee,
      lowTariff:
        (data.consumptionLow || 0) *
        (currentProviderData[0].priceList.lowTariff || 0),
      highTariff:
        data.consumption * currentProviderData[0].priceList.highTariff,
    };

    let totalProviderCostUserTariff = 0;
    for (const [, value] of Object.entries(providerCost)) {
      totalProviderCostUserTariff += value;
    }

    const totalCost =
      totalDistributionCost +
      totalProviderCostUserTariff +
      totalRegulatedServicesCost;

    const totalAnnualCost = (totalCost / data.period) * 365;

    // return all data we collected
    if (!data.isTariffComparison) {
      const totalMonthlyFees =
        monthlyFeeDistributor.monthlyFee +
        regulatedServicesPriceList[0].OTE +
        currentProviderData[0].priceList.monthlyFee +
        POZEBreaker;

      const totalUnitFees =
        (distributorPriceList[0].lowTarif || 0) +
        distributorPriceList[0].highTariff +
        regulatedServicesPriceList[0].tax +
        regulatedServicesPriceList[0].systemServices +
        (currentProviderData[0].priceList.lowTariff || 0) +
        currentProviderData[0].priceList.highTariff +
        POZEConsumption;

      return {
        totalCost: totalCost,
        totalAnnualCost: totalAnnualCost,
        totalMonthlyFees: totalMonthlyFees,
        totalUnitFees: totalUnitFees,
      };
    } else {
      const annualConstantCosts =
        ((totalDistributionCost + totalRegulatedServicesCost) / data.period) *
        365;
      // console.log({ annualconst: annualConstantCosts });

      for (const provider of providerData) {
        const totalProviderCost =
          periodMonths * provider.priceList.monthlyFee +
          (data.consumptionLow || 0) * (provider.priceList.lowTariff || 0) +
          data.consumption * provider.priceList.highTariff;

        const annualProviderCost = (totalProviderCost / data.period) * 365;

        const estimatedCost = annualConstantCosts + annualProviderCost;
        const isBetter = estimatedCost + 1 < totalAnnualCost;
        const savedAmount = totalAnnualCost - estimatedCost;

        // console.log({
        //   [provider.tariff?.name]: {
        //     totalProviderCost,
        //     annualProviderCost,
        //     estimatedCost,
        //     totalAnnualCost,
        //   },
        // });
        const validFrom = admin.firestore.Timestamp.fromMillis(
          provider.tariff?.validFrom.seconds * 1000
        );
        const validTo = admin.firestore.Timestamp.fromMillis(
          provider.tariff?.validTo.seconds * 1000
        );

        const tariff = new Tariff(
          provider.tariff?.uid,
          provider.tariff?.name,
          provider.tariff?.energy,
          0,
          validFrom,
          validTo,
          provider.tariff?.contractDurationMonths,
          provider.tariff?.fixedPriceDurationMonths,
          provider.tariff?.advantages,
          provider.tariff?.disadvantages
        );

        const providerD = new Provider(
          provider.provider?.uid,
          provider.provider?.name,
          provider.provider?.logo,
          provider.provider?.rating
        );
        comparisons.push(
          new Comparison(
            estimatedCost,
            tariff,
            isBetter,
            0,
            providerD,
            savedAmount | 0
          )
        );
      }
      // console.log(comparisons);
      return comparisons;
    }
  }
);
