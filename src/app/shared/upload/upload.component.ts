import { Component, OnInit } from '@angular/core';
import {
  UpdateTariffsService,
  ProviderRaw,
  TariffRaw,
} from 'app/services/update-tariffs.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
})
export class UploadComponent implements OnInit {
  uploadTextarea: string;

  constructor(private up: UpdateTariffsService) {}
  // uploaduje data z excelu AnalyzaElProduktu do db

  ngOnInit(): void {}
  upload() {
    const up = JSON.parse(this.uploadTextarea);
    for (const provider of up) {
      const tar = new TariffRaw(
        provider.tariffs[0].name,
        provider.tariffs[0].validFrom,
        provider.tariffs[0].validTo,
        provider.tariffs[0].contractDurationMonths,
        provider.tariffs[0].fixedPriceDurationMonths,
        provider.tariffs[0].advantages,
        provider.tariffs[0].disadvantages,
        provider.tariffs[0].unitPrice
      );
      const prov = new ProviderRaw(
        provider.name,
        provider.logo,
        provider.rating,
        [tar]
      );
      if (prov instanceof ProviderRaw) {
        this.up.updateTariff(prov);
      } else {
        // console.log('incorrect format');
        // console.log(provider);
      }
    }
  }
}
