import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateTimeAdapter } from 'ng-pick-datetime';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  data: Date = new Date();
  constructor(
    private translate: TranslateService,
    public dateTimeAdapter: DateTimeAdapter<any>
  ) {}

  ngOnInit(): void {}

  useLanguage(language: string) {
    this.translate.use(language);
    this.dateTimeAdapter.setLocale(language);
  }
}
