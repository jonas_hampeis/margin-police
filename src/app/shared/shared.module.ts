import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AbsoluteValuePipe } from 'app/absolute-value.pipe';
import { UploadComponent } from './upload/upload.component';
import { NouisliderModule } from 'ng2-nouislider';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    AbsoluteValuePipe,
    UploadComponent,
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    AngularMultiSelectModule,
    NouisliderModule,
    TranslateModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    NgbModule,
    TranslateModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    NouisliderModule,
    AbsoluteValuePipe,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AngularMultiSelectModule,
  ],
})
export class SharedModule {}
