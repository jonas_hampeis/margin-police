import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  notifications = [
    {
      icon: 'fa-money',
      severity: 'primary',
      text:
        'We found better tariff for Electricity Home - get to know more and save up to 2400 CZK',
    },
    {
      icon: 'fa-bell',
      severity: 'info',
      text: `Set notifications for end of contract at Electricity Home, so you don't miss your chance to switch providers`,
    },
    {
      icon: 'fa-exclamation-triangle',
      severity: 'danger',
      text: `Your gas provider for Gas Home is going to raise prices at 1.7. 2020.
      According to your contract you have 20 days to switch to a different provider.
      Don't miss your chance to get a better tariff!`,
    },
  ];

  pointChecked;

  constructor() {}

  ngOnInit(): void {}
}
