import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/auth/auth.guard';

import { LightComponent } from './light.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SupplyPointComponent } from './supply-point/supply-point.component';
import { TrackingComponent } from './tracking/tracking.component';
import { SupplyPlaceSetComponent } from './supply-place-set/supply-place-set.component';
import { EstimateComponent } from './estimate/estimate.component';
import { LandingComponent } from './landing/landing.component';
import { CompareComponent } from './compare/compare.component';
import { ContractComponent } from './contract/contract.component';
import { TariffSetComponent } from './tariff-set/tariff-set.component';

const routes: Routes = [
  {
    path: '',
    component: LightComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'supply-point', component: SupplyPointComponent },
      { path: 'tracking', component: TrackingComponent },
      { path: 'set', component: SupplyPlaceSetComponent },
      { path: 'estimate', component: EstimateComponent },
      { path: 'landing', component: LandingComponent },
      { path: 'compare', component: CompareComponent },
      { path: 'contract', component: ContractComponent },
      { path: 'set-tariff', component: TariffSetComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LightRoutingModule {}
