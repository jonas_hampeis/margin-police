import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supply-point',
  templateUrl: './supply-point.component.html',
  styleUrls: ['./supply-point.component.scss'],
})
export class SupplyPointComponent implements OnInit {
  editSupplyPoint = false;
  showHistorySupplyPoint = false;
  focus = new Array<boolean>(5);

  constructor() {}

  ngOnInit(): void {}
}
