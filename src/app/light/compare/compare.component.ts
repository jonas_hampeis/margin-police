import { Component, OnInit } from '@angular/core';
import { SlidersConfig } from 'app/police/tariff-compare/sliders.config';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss'],
})
export class CompareComponent implements OnInit {
  ratingConfig: any;
  contractConfig: any;
  priceConfig: any;
  savingsConfig: any;

  tariffCompare = [
    {
      name: 'ČEZ Prodej',
      logo: 'https://www.cez.cz/webpublic/cezw/img/svg/logo-simple.svg',
      default: 0,
      rating: 5,
      tariffs: [
        {
          name: 'Elektřína na 1 rok v akci',
          unitPrice: '5.96',
          validFrom: 1583020800,
          validTo: 1593475200,
          contractDurationMonths: '12 měsíců',
          fixedPriceDurationMonths: '12 měsíců',
          advantages: {
            cs: [
              ' časově omezená akce za výhodnou cenu',
              ' fixace ceny na jeden rok',
              ' stabilní dodavatel',
            ],
            en: [
              ' limited action price',
              ' 1 year price guarantee',
              ' stable and reliable supplier',
            ],
          },
          disadvantages: {
            cs: [' automatické prodloužení smlouvy'],
            en: [' automatic contract prolongation'],
          },
          isBetter: true,
          savedAmount: 1000,
          estimatedCost: 12000,
        },
      ],
    },
    {
      name: 'Innogy',
      logo: 'https://www.innogy.cz/assets/web/img/innogy.svg',
      default: 1,
      rating: 4,
      tariffs: [
        {
          isBetter: true,
          estimatedCost: 12345,
          savedAmount: 500,
          name: 'Elektřina Standard',
          unitPrice: '6.06',
          validFrom: 1582070400,
          validTo: 1609372800,
          contractDurationMonths: 'doba neurčitá',
          fixedPriceDurationMonths: 'ne',
          advantages: {
            cs: [
              ' možnost výpovědi kdykoliv',
              ' mobilní aplikace pro sledování spotřeby',
            ],
            en: [
              ' possibility to leave contract at any time',
              ' mobile app for electricity consumption tracking',
            ],
          },
          disadvantages: {
            cs: [' bez zajištění proti rústu ceny'],
            en: [' without price increase protection'],
          },
        },
      ],
    },
    {
      name: 'E.ON Energie',
      logo:
        'https://tarifomat.cz/static/images/carriers/electricity/carrier_eonenergieas.png',
      default: 0,
      rating: 5,
      tariffs: [
        {
          isBetter: true,
          estimatedCost: 5432,
          savedAmount: 250,
          name: 'Komplet elektřina PRO',
          unitPrice: '5.97',
          validFrom: 1569024000,
          validTo: 1609372800,
          contractDurationMonths: '36 měsíců',
          fixedPriceDurationMonths: '36 měsíců',
          advantages: {
            cs: [
              ' sleva až 1000 z prvního vyúčtování',
              ' fixace ceny',
              ' slevový prohram dodavatele',
              ' až 3000 na elektřikáře',
              ' doporučující program',
            ],
            en: [
              ' discount up to 1000 from first bill',
              ' fixation of price for 3 years',
              ' discount cafeteria',
              ' up tu 3000 CZK for electrician',
              ' referral programme',
            ],
          },
          disadvantages: {
            cs: [' smlouva na 3 roky', ' nevýhodná fixace při poklesu cen'],
            en: [' 3 years contract', ' fixation of price if prices go down'],
          },
        },
      ],
    },
    {
      name: 'E.ON Energie',
      logo:
        'https://tarifomat.cz/static/images/carriers/electricity/carrier_eonenergieas.png',
      default: 0,
      rating: 5,
      tariffs: [
        {
          isBetter: true,
          estimatedCost: 32156,
          savedAmount: 100,
          name: 'Variant 12',
          unitPrice: '5.99',
          validFrom: 1572566400,
          validTo: 1609372800,
          contractDurationMonths: '12 měsíců',
          fixedPriceDurationMonths: 'ne',
          advantages: {
            cs: [
              ' smlouva pouze na 1 rok',
              ' slevový prohram dodavatele',
              ' doporučující program',
            ],
            en: [
              ' 1 year contract only',
              ' discount cafeteria',
              ' referral programme',
            ],
          },
          disadvantages: {
            cs: [' bez zajištění proti rústu ceny'],
            en: [' without price increase protection'],
          },
        },
      ],
    },
    {
      name: 'Yello Energy',
      logo: 'https://kalkulator.tzb-info.cz/img/_/i.000049/yelloenergy_tn.png',
      default: 0,
      rating: 3,
      tariffs: [
        {
          isBetter: true,
          estimatedCost: 43622,
          savedAmount: 400,
          name: 'Yello Watt',
          unitPrice: '5.69',
          validFrom: 1575331200,
          validTo: 1609372800,
          contractDurationMonths: 'doba neurčitá',
          fixedPriceDurationMonths: '12 měsíců',
          advantages: {
            cs: [
              ' smlouva na dobu neurčitou',
              ' garance ceny na 1 rok',
              ' jednoduchá smlouva',
            ],
            en: [
              ' possibility to leave contract at any time',
              ' 1 year price guarantee',
              ' simple contract',
            ],
          },
          disadvantages: { cs: ['-'], en: ['-'] },
        },
      ],
    },
    {
      name: 'Bohemia Energy',
      logo:
        'https://kalkulator.tzb-info.cz/img/_/i.000001/logo_bohemia-energy_tn.jpg',
      default: 0,
      rating: 3,
      tariffs: [
        {
          savedAmount: -2000,
          estimatedCost: 23415,
          isBetter: false,
          name: 'Be Relax Promo 20-21',
          unitPrice: '5.14',
          validFrom: 1583020800,
          validTo: 1590969600,
          contractDurationMonths: '24 měsíců',
          fixedPriceDurationMonths: 'do 31.12.2021',
          advantages: {
            cs: [
              ' cena v závislosti na ceně z velkoobchodního trhu',
              ' časově omezená promo akce',
            ],
            en: [
              ' price is linked to electricity wholesale price',
              ' limited promo offer',
            ],
          },
          disadvantages: {
            cs: [' bez zajištění proti růstu cen', ' smlouva na 24 měsíců'],
            en: [' without price increase protection', ' 24 months contract'],
          },
        },
      ],
    },
  ];
  userTariff = {
    savedAmount: 400,
    name: 'Yello Watt',
    unitPrice: '5.69',
    validFrom: 1575331200,
    validTo: 1609372800,
    contractDurationMonths: 'doba neurčitá',
    fixedPriceDurationMonths: '12 měsíců',
    advantages: {
      cs: [
        ' smlouva na dobu neurčitou',
        ' garance ceny na 1 rok',
        ' jednoduchá smlouva',
      ],
      en: [
        ' possibility to leave contract at any time',
        ' 1 year price guarantee',
        ' simple contract',
      ],
    },
    disadvantages: { cs: ['-'], en: ['-'] },
  };

  constructor(private slidersConfig: SlidersConfig) {
    this.ratingConfig = this.slidersConfig.ratingConfig;
    this.contractConfig = this.slidersConfig.contractConfig;
    this.priceConfig = this.slidersConfig.priceConfig;
    this.savingsConfig = this.slidersConfig.savingsConfig;
  }

  ngOnInit(): void {}
}
