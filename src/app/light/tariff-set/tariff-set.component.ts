import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Tariff } from 'app/models/tariff.model';
import { UserTariff } from 'app/models/userTariff.model';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { TariffService } from 'app/services/tariff.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SupplyPointService } from 'app/services/supply-place.service';
import { map, take } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import * as firebase from 'firebase';
import { PeriodSummary } from 'app/models/periodSummary.model';
import { Provider } from 'app/models/provider.model';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-tariff-set',
  templateUrl: './tariff-set.component.html',
  styleUrls: ['./tariff-set.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.5s ease-out', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('0.5s ease-in', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
export class TariffSetComponent implements OnInit, OnDestroy {
  lang: string;
  langSub: Subscription;

  focus = new Array<boolean>(5);
  tariffsSelectDisable = true;

  placeSelection: string;
  durationSelection: string;

  tariffs: Tariff[];
  tariffsList = [];
  tariff = [];
  tariffsDropdownSettings = {};

  providers: Provider[];
  providersList = [];
  provider = [];
  providersDropdownSettings = {};

  rates: [];
  ratesList = [];
  rate: any;
  ratesDropdownSettings = {};
  ratesSub: Subscription;
  isRateTwoTariff: boolean;

  circuitBreakers: [];
  circuitBreakerList = [];
  circuitBreaker: any;
  circuitBreakersDropdownSettings = {};
  circuitBreakerSub: Subscription;

  consumptionHigh: number;
  consumptionLow: number;
  totalCost: number;
  isTwoTariff: boolean;

  accountingFrom: Date;
  accountingTo: Date;
  definiteDuration: Date;

  addContract = false;
  autoContractExtension: boolean;
  customerNumber: string;

  progressBar = 0;
  progressAdd = [];

  afterSubmitLocation: string;
  isEmptyTariff = false;

  supplyPointUid: string;
  tariffUid: string;

  userTariff: UserTariff;
  supplyPoint: Promise<SupplyPlace>;
  currency: string;

  public selectedMoment = new Date();

  finished = false;

  constructor(
    private translate: TranslateService,
    private tariffService: TariffService,
    private router: Router,
    private route: ActivatedRoute,
    private sps: SupplyPointService
  ) {}
  // nastaveni tarifu

  ngOnInit() {
    this.lang = this.translate.currentLang || 'en';
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.route.queryParams.subscribe((p: Params) => {
      this.isEmptyTariff = true;

      this.userTariff = null;
      this.getProviders();
    });

    // console.log(this.isNewTariff);

    this.provider = [];
    this.providersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectProvider'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect myclass custom-class-example',
      lazyLoading: true,
    };

    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: true,
    };

    // transformace sazeb a jisticu z ciselniku na format pouzitelny dropdown komponentou
    this.rate = [];
    this.ratesSub = this.tariffService.rates
      .pipe(
        map((rates) => {
          const rtList = [];
          rates.sort((a, b) => {
            if (a.rate < b.rate) {
              return -1;
            }
            if (a.rate > b.rate) {
              return 1;
            }
            return 0;
          });

          for (const rate of rates) {
            rtList.push({
              id: rate.id,
              itemName: rate.rate + ' - ' + rate.description[this.lang],
              isTwoTariff: rate.isTwoTariff,
            });
          }

          return rtList;
        })
      )
      .subscribe((res) => {
        this.ratesList = res;
      });

    // console.log(this.ratesList);
    this.ratesDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.setTariff.rates'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: false,
    };

    this.circuitBreaker = [];

    this.circuitBreakerSub = this.tariffService.circuitBreakers
      .pipe(
        map((circuitBreakers) => {
          const cbList = [];
          circuitBreakers.sort((a, b) => {
            if (+a.id < +b.id) {
              return -1;
            }
            if (+a.id > +b.id) {
              return 1;
            }
            return 0;
          });

          for (const circuitBreaker of circuitBreakers) {
            cbList.push({
              id: circuitBreaker.id,
              itemName: circuitBreaker.description[this.lang],
            });
          }

          return cbList;
        })
      )
      .subscribe((res) => {
        // console.log(res);
        this.circuitBreakerList = res;
      });

    // const cbList = this.translate.instant(
    //   'police.setTariff.circuitBreakerList'
    // );
    // // console.log(cbList);
    // for (let i = 0; i < cbList.length; i++) {
    //   this.circuitBreakerList.push({ id: i, itemName: cbList[i] });
    // }
    this.circuitBreakersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.setTariff.circuitBreaker'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: false,
    };
  }

  onRadioClick() {
    this.progressBarAdd('1');
    this.provider = [];
    this.tariff = [];
    this.getProviders();
  }

  async getProviders() {
    await this.tariffService
      .getProviders(this.placeSelection)
      .toPromise()
      .then((data) => {
        this.providers = data;
      });

    this.providersList = this.providers.map((a) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  async getTariffs(provider: string) {
    this.tariffs = await this.tariffService
      .getTariffs(provider, 'electricity')
      .pipe(take(1))
      .toPromise();

    this.tariffsList = this.tariffs.map((a: Tariff) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  onProviderSelect() {
    this.progressBarAdd('2');
    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: false,
    };

    this.getTariffs(this.provider[0].id);
    // console.log(this.tariff);
  }
  onProviderDeselect() {
    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: true,
    };
  }

  onTariffSelect() {
    this.progressBarAdd('4');
    this.currency = this.tariffs.find((e) => {
      return e.uid === this.tariff[0].id && e.name === this.tariff[0].itemName;
    }).currency;
  }

  onRateSelect(rate: any) {
    this.isRateTwoTariff = rate.isTwoTariff;
    this.isTwoTariff = rate.isTwoTariff;
  }

  progressBarAdd(id: string) {
    const totalElem = 12;
    const prog = 100 / totalElem;
    if (!this.progressAdd.includes(id)) {
      this.progressBar += prog;
      if (this.progressBar > 98) {
        this.progressBar = 100;
      }
      this.progressAdd.push(id);
    }
  }

  onClickAddContract() {
    this.addContract = !this.addContract;
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.langSub.unsubscribe();
  }
}
