import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-estimate',
  templateUrl: './estimate.component.html',
  styleUrls: ['./estimate.component.scss'],
})
export class EstimateComponent implements OnInit {
  focus = new Array<boolean>(99);
  peopleConfig: any = {
    start: 1,
    direction: 'ltr',
    range: {
      min: 1,
      max: 4,
    },
    tooltips: true,
    step: 1,
  };

  usages = [
    {
      name: 'police.estimate.light',
      value: 'light',
      checked: true,
      icon: 'fa-lightbulb-o',
    },
    {
      name: 'police.estimate.cook',
      value: 'cook',
      checked: false,
      icon: 'fa-cutlery',
    },
    {
      name: 'police.estimate.waterHeating',
      value: 'waterHeating',
      checked: false,
      icon: 'fa-tint',
    },
    {
      name: 'police.estimate.heating',
      value: 'heating',
      checked: false,
      icon: 'fa-fire',
    },
    {
      name: 'police.estimate.heatPump',
      value: 'heatPump',
      checked: false,
      icon: 'fa-thermometer-full',
    },
    {
      name: 'police.estimate.carCharging',
      value: 'carCharging',
      checked: false,
      icon: 'fa-car',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
