import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LightComponent } from './light.component';
import { RouterModule } from '@angular/router';
import { LightRoutingModule } from './light.routing';
import { SharedModule } from 'app/shared/shared.module';
import { SupplyPointComponent } from './supply-point/supply-point.component';
import { TrackingComponent } from './tracking/tracking.component';
import { SupplyPlaceSetComponent } from './supply-place-set/supply-place-set.component';
import { EstimateComponent } from './estimate/estimate.component';
import { LandingComponent } from './landing/landing.component';
import { CompareComponent } from './compare/compare.component';
import { ContractComponent } from './contract/contract.component';
import { TariffSetComponent } from './tariff-set/tariff-set.component';

@NgModule({
  declarations: [
    DashboardComponent,
    LightComponent,
    SupplyPointComponent,
    TrackingComponent,
    SupplyPlaceSetComponent,
    EstimateComponent,
    LandingComponent,
    CompareComponent,
    ContractComponent,
    TariffSetComponent,
  ],
  imports: [CommonModule, RouterModule, LightRoutingModule, SharedModule],
})
export class LightModule {}
