import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  energySelection = {
    electricity: {
      name: 'Electricity',
      value: 'electricity',
      checked: false,
      icon: 'fa-bolt',
    },
    gas: {
      name: 'Gas',
      value: 'gas',
      checked: false,
      icon: 'fa-fire',
    },
  };
  selectedEnergy: string;

  constructor() {}

  ngOnInit(): void {}

  onEnergyClick(key: string) {
    this.energySelection[key].checked = !this.energySelection[key].checked;
    if (key === 'gas') {
      this.energySelection.electricity.checked = false;
    } else {
      this.energySelection.gas.checked = false;
    }
    this.selectedEnergy = key;
  }
}
