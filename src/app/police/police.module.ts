import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PoliceComponent } from './police.component';
import { TariffsComponent } from './tariffs/tariffs.component';
import { TariffComponent } from './tariff/tariff.component';
import { PoliceRoutingModule } from './police.routing';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { TariffCompareComponent } from './tariff-compare/tariff-compare.component';
import { AbsoluteValuePipe } from '../absolute-value.pipe';
import { TrackLessComponent } from './track/track.component';
import { SetTariffComponent } from './set-tariff/set-tariff.component';
import { IndicativeEstimateComponent } from './indicative-estimate/indicative-estimate.component';
import { RecapitulationComponent } from './recapitulation/recapitulation.component';
import { SupplyPlacesComponent } from './supply-places/supply-places.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SetComponent } from './supply-places/set/set.component';
import { UploadDocsComponent } from './upload-docs/upload-docs.component';

@NgModule({
  declarations: [
    PoliceComponent,
    TariffsComponent,
    TariffComponent,
    TariffCompareComponent,
    TrackLessComponent,
    SetTariffComponent,
    IndicativeEstimateComponent,
    RecapitulationComponent,
    SupplyPlacesComponent,
    NotificationsComponent,
    SetComponent,
    UploadDocsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    PoliceRoutingModule,
    NgbModule,
    FormsModule,
    SharedModule,
  ],
  providers: [AbsoluteValuePipe],
})
export class PoliceModule {}
