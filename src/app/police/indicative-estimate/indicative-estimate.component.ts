import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { trigger, transition, style, animate } from '@angular/animations';
import { Provider } from 'app/models/provider.model';
import { TariffService } from 'app/services/tariff.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SupplyPointService } from 'app/services/supply-place.service';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { EstimatedTariff } from 'app/models/estimatedTariff.model';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-indicative-estimate',
  templateUrl: './indicative-estimate.component.html',
  styleUrls: ['./indicative-estimate.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.5s ease-out', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('0.5s ease-in', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
export class IndicativeEstimateComponent implements OnInit, OnDestroy {
  focus = new Array<boolean>(5);
  placeSelection: 'household' | 'business';
  progressBar = 0;
  progressAdd = [];

  peopleConfig: any = {
    start: 1,
    direction: 'ltr',
    range: {
      min: 1,
      max: 4,
    },
    tooltips: true,
    step: 1,
  };
  people = 1;

  showUsageSelection = false;

  model = {
    left: true,
    middle: false,
    right: false,
  };

  usages = [
    {
      name: 'police.estimate.light',
      value: 'light',
      checked: true,
      icon: 'fa-lightbulb-o',
    },
    {
      name: 'police.estimate.cook',
      value: 'cook',
      checked: false,
      icon: 'fa-cutlery',
    },
    {
      name: 'police.estimate.waterHeating',
      value: 'waterHeating',
      checked: false,
      icon: 'fa-tint',
    },
    {
      name: 'police.estimate.heating',
      value: 'heating',
      checked: false,
      icon: 'fa-fire',
    },
    {
      name: 'police.estimate.heatPump',
      value: 'heatPump',
      checked: false,
      icon: 'fa-thermometer-full',
    },
    {
      name: 'police.estimate.carCharging',
      value: 'carCharging',
      checked: false,
      icon: 'fa-car',
    },
  ];
  objectType: 'flat' | 'house' | 'weekendCottage';

  showProvider = false;
  providers: Provider[];
  providersList = [];
  provider = [];
  providersDropdownSettings = {};

  deposit: number;

  showProgress = true;

  paramsSub: Subscription;
  supplyPointUid: string;
  tariffUid: string;
  supplyPoint: Promise<SupplyPlace>;
  afterSubmitLocation: string;

  constructor(
    private tariffService: TariffService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private sps: SupplyPointService,
    private router: Router
  ) {}
  // odhadnuti tarifu
  // nic negeneruje, ukladaji se akorat vyplnene informace

  ngOnInit() {
    this.paramsSub = this.route.queryParams.subscribe((params: Params) => {
      // console.log(params.version);
      this.supplyPointUid = params.supplyPoint;
      this.tariffUid = params.tariff;
      if (this.supplyPointUid && this.tariffUid) {
        this.getSupplyPlace();
      } else {
        this.router.navigate(['/police']);
      }
    });

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.getProviders();

    this.provider = [];
    this.providersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectProvider'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect myclass custom-class-example',
      lazyLoading: true,
    };
  }

  async getSupplyPlace() {
    this.supplyPoint = (await this.sps.getSupplyPlace(this.supplyPointUid))
      .pipe(take(1))
      .toPromise();
  }

  async getProviders() {
    await this.tariffService
      .getProviders(this.placeSelection)
      .toPromise()
      .then((data) => {
        this.providers = data;
      });

    this.providersList = this.providers.map((a) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  progressBarAdd(id: string) {
    const totalElem = 6;
    const prog = Math.round(100 / totalElem);
    if (!this.progressAdd.includes(id)) {
      this.progressBar += prog;
      if (this.progressBar > 98) {
        this.progressBar = 100;
      }
      this.progressAdd.push(id);
    }
  }

  getIndex(index) {
    return index;
  }

  onSubmit(form: NgForm) {
    // console.log(form);
    this.showProgress = false;

    const selectedUsages = new Array<string>();
    for (const usage of this.usages) {
      if (usage.checked) {
        selectedUsages.push(usage.value);
      }
    }

    let selectedProvider: Provider;
    if (form.value.provider.length > 0) {
      selectedProvider = this.providers.find((e) => {
        return (
          e.uid === form.value.provider[0].id &&
          e.name === form.value.provider[0].itemName
        );
      });
    }

    const estimatedTariff = new EstimatedTariff(
      this.placeSelection,
      this.objectType,
      this.people,
      selectedUsages || null,
      selectedProvider || null,
      this.deposit || null
    );

    this.tariffService.setEstimatedTariff(
      this.tariffUid,
      estimatedTariff,
      this.supplyPointUid
    );
    // this.router.navigate([`/police/${this.afterSubmitLocation}`], {
    //   queryParamsHandling: 'merge',
    // });
  }

  setRouter(to: string) {
    this.afterSubmitLocation = to;
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
