import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicativeEstimateComponent } from './indicative-estimate.component';

describe('IndicativeEstimateComponent', () => {
  let component: IndicativeEstimateComponent;
  let fixture: ComponentFixture<IndicativeEstimateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicativeEstimateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicativeEstimateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
