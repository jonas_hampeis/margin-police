import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Tariff } from 'app/models/tariff.model';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { TariffService } from 'app/services/tariff.service';
import { Provider } from 'app/models/provider.model';
import { trigger, transition, style, animate } from '@angular/animations';
import { UserTariff } from 'app/models/userTariff.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { take, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { PeriodSummary } from 'app/models/periodSummary.model';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { SupplyPointService } from 'app/services/supply-place.service';

@Component({
  selector: 'app-set-tariff',
  templateUrl: './set-tariff.component.html',
  styleUrls: ['./set-tariff.component.scss'],
  animations: [
    trigger('inOutAnimation', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('0.5s ease-out', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        style({ opacity: 1 }),
        animate('0.5s ease-in', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
export class SetTariffComponent implements OnInit, OnDestroy {
  lang: string;
  langSub: Subscription;

  focus = new Array<boolean>(5);
  tariffsSelectDisable = true;

  placeSelection: string;
  durationSelection: string;

  tariffs: Tariff[];
  tariffsList = [];
  tariff = [];
  tariffsDropdownSettings = {};

  providers: Provider[];
  providersList = [];
  provider = [];
  providersDropdownSettings = {};

  rates: [];
  ratesList = [];
  rate: any;
  ratesDropdownSettings = {};
  ratesSub: Subscription;
  isRateTwoTariff: boolean;

  circuitBreakers: [];
  circuitBreakerList = [];
  circuitBreaker: any;
  circuitBreakersDropdownSettings = {};
  circuitBreakerSub: Subscription;

  consumptionHigh: number;
  consumptionLow: number;
  totalCost: number;
  isTwoTariff: boolean;

  accountingFrom: Date;
  accountingTo: Date;
  definiteDuration: Date;

  addContract = false;
  autoContractExtension: boolean;
  customerNumber: string;

  progressBar = 0;
  progressAdd = [];

  afterSubmitLocation: string;
  isEmptyTariff = false;

  supplyPointUid: string;
  tariffUid: string;

  userTariff: UserTariff;
  supplyPoint: Promise<SupplyPlace>;
  currency: string;

  public selectedMoment = new Date();

  finished = false;

  constructor(
    private translate: TranslateService,
    private tariffService: TariffService,
    private router: Router,
    private route: ActivatedRoute,
    private sps: SupplyPointService
  ) {}
  // nastaveni tarifu

  ngOnInit() {
    this.lang = this.translate.currentLang || 'en';
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.route.queryParams.subscribe((p: Params) => {
      if (p.isEmptyTariff === 'true') {
        this.isEmptyTariff = true;
      }

      this.supplyPointUid = p.supplyPoint;
      this.tariffUid = p.tariff;

      this.getSupplyPlace();

      if (this.isEmptyTariff) {
        this.userTariff = null;
        this.getProviders();
      } else {
        this.loadTariff(this.supplyPointUid, this.tariffUid);
      }
    });

    // console.log(this.isNewTariff);

    this.provider = [];
    this.providersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectProvider'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect myclass custom-class-example',
      lazyLoading: true,
    };

    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: true,
    };

    // transformace sazeb a jisticu z ciselniku na format pouzitelny dropdown komponentou
    this.rate = [];
    this.ratesSub = this.tariffService.rates
      .pipe(
        map((rates) => {
          const rtList = [];
          rates.sort((a, b) => {
            if (a.rate < b.rate) {
              return -1;
            }
            if (a.rate > b.rate) {
              return 1;
            }
            return 0;
          });

          for (const rate of rates) {
            rtList.push({
              id: rate.id,
              itemName: rate.rate + ' - ' + rate.description[this.lang],
              isTwoTariff: rate.isTwoTariff,
            });
          }

          return rtList;
        })
      )
      .subscribe((res) => {
        this.ratesList = res;
      });

    // console.log(this.ratesList);
    this.ratesDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.setTariff.rates'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: false,
    };

    this.circuitBreaker = [];

    this.circuitBreakerSub = this.tariffService.circuitBreakers
      .pipe(
        map((circuitBreakers) => {
          const cbList = [];
          circuitBreakers.sort((a, b) => {
            if (+a.id < +b.id) {
              return -1;
            }
            if (+a.id > +b.id) {
              return 1;
            }
            return 0;
          });

          for (const circuitBreaker of circuitBreakers) {
            cbList.push({
              id: circuitBreaker.id,
              itemName: circuitBreaker.description[this.lang],
            });
          }

          return cbList;
        })
      )
      .subscribe((res) => {
        // console.log(res);
        this.circuitBreakerList = res;
      });

    // const cbList = this.translate.instant(
    //   'police.setTariff.circuitBreakerList'
    // );
    // // console.log(cbList);
    // for (let i = 0; i < cbList.length; i++) {
    //   this.circuitBreakerList.push({ id: i, itemName: cbList[i] });
    // }
    this.circuitBreakersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.setTariff.circuitBreaker'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: false,
    };
  }

  async getSupplyPlace() {
    this.supplyPoint = (await this.sps.getSupplyPlace(this.supplyPointUid))
      .pipe(take(1))
      .toPromise();
  }

  // pokud vstupujeme do komponenty s existujicim tarifem, nactou se do poli prislusne udaje
  async loadTariff(supplyPoint: string, tariff: string) {
    // console.log('loading tariff');
    this.userTariff = await (
      await this.tariffService.getUserTariff(supplyPoint, tariff)
    )
      .pipe(take(1))
      .toPromise();

    // console.log(this.userTariff);
    // this.currency = this.userTariff.currency;

    this.placeSelection = this.userTariff.place;

    this.getProviders();
    this.provider.push({
      id: this.userTariff.providerUid,
      itemName: this.userTariff.providerUid,
    });
    this.onProviderSelect();

    this.tariff.push({
      id: this.userTariff.name,
      itemName: this.userTariff.name,
    });

    this.tariffService.rates
      .pipe(
        map((d) => {
          // console.log(d);
          for (const rate of d) {
            if (+rate.id === +this.userTariff.rate) {
              return rate;
            }
          }
          return {
            id: -1,
            rate: '',
            description: {
              [this.lang]: 'Something went wrong. Choose circuit breaker again',
            },
          };
        })
      )
      .subscribe((r) => {
        // console.log(r);
        this.rate.push({
          id: r.id,
          itemName: r.rate + ' - ' + r.description[this.lang],
        });
      });

    this.tariffService.circuitBreakers
      .pipe(
        map((d) => {
          // console.log(d);
          for (const breaker of d) {
            if (+breaker.id === +this.userTariff.circuitBreaker) {
              return breaker;
            }
          }
          return {
            id: -1,
            description: {
              [this.lang]: 'Something went wrong. Choose circuit breaker again',
            },
          };
        })
      )
      .subscribe((r) => {
        // console.log(r);
        this.circuitBreaker.push({
          id: r.id,
          itemName: r.description[this.lang],
        });
      });

    this.consumptionHigh = Math.round(
      this.userTariff.periodSummary.annualConsumption
    );
    this.consumptionLow = Math.round(
      this.userTariff.periodSummary.annualConsumptionLow
    );
    this.totalCost = Math.round(this.userTariff.periodSummary.annualCost);
    this.isTwoTariff = this.isRateTwoTariff = this.userTariff.periodSummary.isTwoTariff;

    for (let i = 1; i <= 8; ++i) {
      this.progressBarAdd(i.toString());
    }

    if (
      this.userTariff.accountingFrom &&
      (this.accountingFrom = new Date(
        this.userTariff.accountingFrom.seconds * 1000
      ))
    ) {
      this.progressBarAdd('9');
    }
    if (
      this.userTariff.accountingTo &&
      (this.accountingTo = new Date(
        this.userTariff.accountingTo.seconds * 1000
      ))
    ) {
      this.progressBarAdd('10');
    }

    if (this.userTariff.contractDuration) {
      this.addContract = true;
      if ((this.durationSelection = this.userTariff.contractDuration)) {
        this.progressBarAdd('12');
      }
      this.definiteDuration = this.userTariff.definiteDuration;
      this.autoContractExtension = this.userTariff.autoContractExtension;
      if ((this.customerNumber = this.userTariff.customerNumber)) {
        this.progressBarAdd('13');
      }
    }
  }

  onRadioClick() {
    this.progressBarAdd('1');
    this.provider = [];
    this.tariff = [];
    this.getProviders();
  }

  async getProviders() {
    await this.tariffService
      .getProviders(this.placeSelection)
      .toPromise()
      .then((data) => {
        this.providers = data;
      });

    this.providersList = this.providers.map((a) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  async getTariffs(provider: string) {
    this.tariffs = await this.tariffService
      .getTariffs(provider, 'electricity')
      .pipe(take(1))
      .toPromise();

    this.tariffsList = this.tariffs.map((a: Tariff) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  onProviderSelect() {
    this.progressBarAdd('2');
    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: false,
    };

    this.getTariffs(this.provider[0].id);
    // console.log(this.tariff);
  }
  onProviderDeselect() {
    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: true,
    };
  }

  onTariffSelect() {
    this.progressBarAdd('4');
    this.currency = this.tariffs.find((e) => {
      return e.uid === this.tariff[0].id && e.name === this.tariff[0].itemName;
    }).currency;
  }

  onRateSelect(rate: any) {
    this.isRateTwoTariff = rate.isTwoTariff;
    this.isTwoTariff = rate.isTwoTariff;
  }

  progressBarAdd(id: string) {
    const totalElem = 12;
    const prog = 100 / totalElem;
    if (!this.progressAdd.includes(id)) {
      this.progressBar += prog;
      if (this.progressBar > 98) {
        this.progressBar = 100;
      }
      this.progressAdd.push(id);
    }
  }

  onClickAddContract() {
    this.addContract = !this.addContract;
  }

  onSubmit(form: NgForm) {
    const selectedTariff = this.tariffs.find((e) => {
      return (
        e.uid === form.value.tariff[0].id &&
        e.name === form.value.tariff[0].itemName
      );
    });
    const accFrom = this.accountingFrom
      ? firebase.firestore.Timestamp.fromDate(this.accountingFrom)
      : null;
    const accTo = this.accountingTo
      ? firebase.firestore.Timestamp.fromDate(this.accountingTo)
      : null;
    const userTariff = new UserTariff(
      null,
      selectedTariff.name,
      selectedTariff.unitPrice,
      selectedTariff.validFrom,
      selectedTariff.validTo,
      form.value.placeSelection,
      +form.value.rate[0].id,
      +form.value.circuitBreaker[0].id,
      'electricity',
      accFrom || null,
      accTo || null,
      form.value.durationSelection || null,
      form.value.customerNumber || '',
      this.definiteDuration || null,
      form.value.autoContractExtension || null,
      new PeriodSummary(
        'billing',
        null,
        null,
        form.value.consumptionHigh,
        form.value.totalCost || 0,
        this.isTwoTariff,
        null,
        form.value.consumptionLow
      ),
      form.value.provider[0].itemName,
      selectedTariff.currency,
      selectedTariff.unit,
      'billing'
    );
    if (!form.value.consumptionHigh || !form.value.totalCost) {
      userTariff.periodSummary.from = null;
    }

    // console.log(userTariff);
    this.tariffService
      .setTariff(this.tariffUid, userTariff, this.supplyPointUid)
      .then(() =>
        this.router.navigate([`/police/${this.afterSubmitLocation}`], {
          queryParams: {
            supplyPoint: this.supplyPointUid,
            tariff: this.tariffUid,
          },
        })
      );
    this.sps.setCircuitBreaker(
      this.supplyPointUid,
      +form.value.circuitBreaker[0].id
    );
    // }
  }

  setRouter(to: string) {
    this.finished = true;
    this.afterSubmitLocation = to;
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.langSub.unsubscribe();
  }
}
