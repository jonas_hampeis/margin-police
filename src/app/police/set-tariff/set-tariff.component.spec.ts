import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetTariffComponent } from './set-tariff.component';

describe('SetTariffComponent', () => {
  let component: SetTariffComponent;
  let fixture: ComponentFixture<SetTariffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetTariffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetTariffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
