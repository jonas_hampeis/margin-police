import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TariffCompareComponent } from './tariff-compare.component';

describe('TariffCompareComponent', () => {
  let component: TariffCompareComponent;
  let fixture: ComponentFixture<TariffCompareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TariffCompareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TariffCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
