import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrackingService } from 'app/services/tracking.service';
import { Subscription } from 'rxjs';
import { Comparison } from 'app/models/comparison.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { NgbTabChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'app/models/user.model';
import { take } from 'rxjs/operators';
import { SlidersConfig } from './sliders.config';
import { TariffService } from 'app/services/tariff.service';
import { UserTariff } from 'app/models/userTariff.model';
import { SupplyPointService } from 'app/services/supply-place.service';
import { SupplyPlace } from 'app/models/supplyPlace.model';

@Component({
  selector: 'app-tariff-compare',
  templateUrl: './tariff-compare.component.html',
  styleUrls: ['./tariff-compare.component.scss'],
})
export class TariffCompareComponent implements OnInit, OnDestroy {
  // full array of all tariffs and comparisons
  tariffCompareFull: Comparison[];
  // used for filtering
  tariffCompare: Comparison[];
  paramsSub: Subscription;
  params: string;
  lang: string;
  langSub: Subscription;
  storage: firebase.storage.Storage;
  img: firebase.storage.Reference;
  user: User;
  rating: number;
  savings = [0, 10000];
  contractDuration = [-1, 3];
  fixedPrice = [0, 4];
  ratingConfig: any;
  contractConfig: any;
  priceConfig: any;
  savingsConfig: any;

  supplyPointUid: string;
  tariffUid: string;
  userTariff: UserTariff;
  supplyPoint: Promise<SupplyPlace>;
  isDefault: boolean;

  constructor(
    private trs: TrackingService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private slidersConfig: SlidersConfig,
    private tfs: TariffService,
    private sps: SupplyPointService
  ) {
    this.ratingConfig = this.slidersConfig.ratingConfig;
    this.contractConfig = this.slidersConfig.contractConfig;
    this.priceConfig = this.slidersConfig.priceConfig;
    this.savingsConfig = this.slidersConfig.savingsConfig;
  }
  // porovnani tarifu
  // samotne porovnani probiha v cloud functions
  // toto jenom nacita a zobrazuje
  // plus zarizuje filtrovani a razeni

  ngOnInit() {
    this.paramsSub = this.route.queryParams.subscribe((params: Params) => {
      this.params = params.version;
      this.supplyPointUid = params.supplyPoint;
      this.tariffUid = params.tariff;
      if (this.supplyPointUid && this.tariffUid) {
        this.getUserTariff();
        this.getCompareData();
        this.getSupplyPlace();
      } else {
        this.router.navigate(['/police']);
      }
    });

    this.lang = this.translate.currentLang || 'en';
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  async getSupplyPlace() {
    this.supplyPoint = (await this.sps.getSupplyPlace(this.supplyPointUid))
      .pipe(take(1))
      .toPromise();
  }

  async getUserTariff() {
    this.userTariff = await (
      await this.tfs.getUserTariff(this.supplyPointUid, this.tariffUid)
    )
      .pipe(take(1))
      .toPromise();
  }

  async getCompareData() {
    let comparisons: { comparisons: Comparison[]; defaults: boolean };

    const revData = await (
      await this.trs.getPeriodSummary(this.supplyPointUid, this.tariffUid)
    )
      .pipe(take(1))
      .toPromise();
    comparisons = await this.trs.compareTariffs(revData.calcData);

    if (!Array.isArray(comparisons.comparisons)) {
      console.log('something went wrong');
      this.router.navigate(['/police']);
      return;
    }

    this.isDefault = comparisons.defaults;
    this.tariffCompare = this.tariffCompareFull = comparisons.comparisons;
    this.sortTariffs();
  }

  languageFallback(obj: Object) {
    return Object.keys(obj)[0];
  }

  sortTariffs(event?: NgbTabChangeEvent) {
    let switcher = '';
    if (event) {
      switcher = event.nextId;
    }
    switch (switcher) {
      case 'rating':
        this.tariffCompare.sort((a, b) =>
          a.provider.rating < b.provider.rating
            ? 1
            : b.provider.rating < a.provider.rating
            ? -1
            : 0
        );
        break;
      case 'provider':
        this.tariffCompare.sort((a, b) =>
          a.provider.name.localeCompare(b.provider.name)
        );
        break;
      default:
        this.tariffCompare.sort((a, b) =>
          a.savedAmount < b.savedAmount
            ? 1
            : b.savedAmount < a.savedAmount
            ? -1
            : 0
        );
        break;
    }
  }

  filterTariffs(filter: string) {
    switch (filter) {
      case 'rating': {
        this.rating = this.rating || 0;
        this.tariffCompare = this.tariffCompareFull.filter(
          (tf) => tf.provider.rating >= this.rating
        );
        break;
      }
      case 'contractDuration': {
        const minCon = this.contractDuration[0] * 12;
        const maxCon =
          this.contractDuration[1] >= 3
            ? Number.MAX_SAFE_INTEGER
            : this.contractDuration[1] * 12;
        this.tariffCompare = this.tariffCompareFull.filter(
          (tf) =>
            tf.tariff.contractDurationMonths >= minCon &&
            tf.tariff.contractDurationMonths <= maxCon
        );
        break;
      }
      case 'fixedPrice': {
        const minFix = this.fixedPrice[0] * 12;
        const maxFix =
          this.fixedPrice[1] >= 4
            ? Number.MAX_SAFE_INTEGER
            : this.fixedPrice[1] * 12;
        this.tariffCompare = this.tariffCompareFull.filter(
          (tf) =>
            tf.tariff.fixedPriceDurationMonths >= minFix &&
            tf.tariff.fixedPriceDurationMonths <= maxFix
        );
        break;
      }
      case 'savings': {
        const maxSave =
          this.savings[1] >= 10000 ? Number.MAX_SAFE_INTEGER : this.savings[1];
        this.tariffCompare = this.tariffCompareFull.filter(
          (tf) => tf.savedAmount >= this.savings[0] && tf.savedAmount <= maxSave
        );
        break;
      }
    }
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');

    if (this.paramsSub) {
      this.paramsSub.unsubscribe();
    }
    if (this.langSub) {
      this.langSub.unsubscribe();
    }
  }
}
