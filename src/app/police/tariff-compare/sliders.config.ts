import { NouiFormatter } from 'ng2-nouislider';
import { Injectable } from '@angular/core';

export class RatingFormatter implements NouiFormatter {
  to(value: number): string {
    return '>= ' + value;
  }

  from(value: string): number {
    return Number(value.replace('>= ', ''));
  }
}
export class YearsFormatterContract implements NouiFormatter {
  to(value: number): string {
    switch (value) {
      case -1:
        return 'Indefinite';
      case 1:
        return value + ' year';
      case 3:
        return value + ' years +';
      default:
        return value + ' years';
    }
  }

  from(value: string): number {
    return Number(value.replace(' years', ''));
  }
}
export class YearsFormatterPrice implements NouiFormatter {
  to(value: number): string {
    switch (value) {
      case 0:
        return 'Not fixed';
      case 1:
        return value + ' year';
      case 4:
        return 'Indefinite';
      default:
        return value + ' years';
    }
  }

  from(value: string): number {
    return Number(value.replace(' years', ''));
  }
}
export class PriceFormatter implements NouiFormatter {
  to(value: number): string {
    if (value === 10000) {
      return '10 000+ CZK';
    } else {
      return Math.floor(value) + ' CZK';
    }
  }

  from(value: string): number {
    return Number(value.replace(' CZK', ''));
  }
}

@Injectable({
  providedIn: 'root',
})
export class SlidersConfig {
  public ratingConfig: any = {
    start: 0,
    connect: 'upper',
    direction: 'rtl',
    range: {
      min: 0,
      max: 5,
    },
    tooltips: new RatingFormatter(),
    step: 1,
  };
  public contractConfig: any = {
    start: [-1, 3],
    connect: [false, true, false],
    range: {
      min: -1,
      max: 3,
    },
    tooltips: [new YearsFormatterContract(), new YearsFormatterContract()],
    step: 1,
  };
  public priceConfig: any = {
    start: [0, 4],
    connect: [false, true, false],
    range: {
      min: 0,
      max: 4,
    },
    tooltips: [new YearsFormatterPrice(), new YearsFormatterPrice()],
    step: 1,
  };
  public savingsConfig: any = {
    start: [0, 10000],
    connect: [false, true, false],
    range: {
      min: 0,
      max: 10000,
    },
    tooltips: [new PriceFormatter(), new PriceFormatter()],
    step: 1,
  };
}
