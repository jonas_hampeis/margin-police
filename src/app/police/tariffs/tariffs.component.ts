import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { TariffService } from 'app/services/tariff.service';
import { Provider } from 'app/models/provider.model';
import { Tariff } from 'app/models/tariff.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tariffs',
  templateUrl: './tariffs.component.html',
  styleUrls: ['./tariffs.component.scss'],
})
export class TariffsComponent implements OnInit, OnDestroy {
  focus: boolean;
  tariffsSelectDisable = true;

  energySelection = 'electricity';

  tariffs: Tariff[];
  tariffsList = [];
  tariff = [];
  tariffsDropdownSettings = {};

  providers: Provider[];
  providersList = [];
  provider = [];
  providersDropdownSettings = {};

  constructor(
    private translate: TranslateService,
    private tariffService: TariffService,
    private router: Router
  ) {}
  // zastarala komponenta pro vyber tarifu
  // pouziva se komponenta ve slozce set-tariff

  onRadioClick() {
    this.provider = [];
    this.tariff = [];
    this.getProviders();
  }

  async getProviders() {
    await this.tariffService
      .getProviders(this.energySelection)
      .toPromise()
      .then((data) => {
        this.providers = data;
      });

    this.providersList = this.providers.map((a) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  async getTariffs(provider: string) {
    await this.tariffService
      .getTariffs(provider, this.energySelection)
      .toPromise()
      .then((data) => {
        this.tariffs = data;
      });

    this.tariffsList = this.tariffs.map((a: Tariff) => {
      return { id: a.uid, itemName: a.name };
    });
  }

  onProviderSelect() {
    this.tariff = null;
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: false,
    };

    this.getTariffs(this.provider[0].id);
  }
  onProviderDeselect() {
    this.tariff = null;
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: true,
    };
  }

  onSubmit(form: NgForm) {
    // const selectedTariff = this.tariffs.find((e) => {
    //   return (
    //     e.uid === form.value.tariff[0].id &&
    //     e.name === form.value.tariff[0].itemName
    //   );
    // });
    // this.tariffService
    //   .setTariff(selectedTariff, form.value.postalCode)
    //   .then(() => this.router.navigate(['/police/track']));
  }

  ngOnInit() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.getProviders();

    this.provider = [];
    this.providersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectProvider'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
    };

    this.tariff = [];
    this.tariffsDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.tariffs.selectTariff'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: '',
      lazyLoading: true,
      disabled: true,
    };
  }
  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
