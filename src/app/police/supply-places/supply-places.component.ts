import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { take } from 'rxjs/operators';
import { UserTariff } from 'app/models/userTariff.model';
import { User } from 'app/models/user.model';
import { Observable, Subscription } from 'rxjs';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { SupplyPointService } from 'app/services/supply-place.service';

@Component({
  selector: 'app-supply-points',
  templateUrl: './supply-places.component.html',
  styleUrls: ['./supply-places.component.scss'],
})
export class SupplyPlacesComponent implements OnInit, OnDestroy {
  user: User;
  userTariff: UserTariff[];
  $supplyPoints: Observable<SupplyPlace[]>;
  pointChecked = [];
  selectedPoint: string;

  lang: string;
  langSub: Subscription;

  notifications = [
    {
      icon: 'fa-money',
      severity: 'primary',
      text:
        'We found better tariff for Electricity Home - get to know more and save up to 2400 CZK',
    },
    {
      icon: 'fa-bell',
      severity: 'info',
      text: `Set notifications for end of contract at Electricity Home, so you don't miss your chance to switch providers`,
    },
    {
      icon: 'fa-exclamation-triangle',
      severity: 'danger',
      text: `Your gas provider for Gas Home is going to raise prices at 1.7. 2020.
      According to your contract you have 20 days to switch to a different provider.
      Don't miss your chance to get a better tariff!`,
    },
  ];

  constructor(
    private auth: AuthService,
    private translate: TranslateService,
    private supplyService: SupplyPointService
  ) {}
  // komponenta pro praci s odbernymi misty
  // funguje spis jako dashboard cele aplikace

  ngOnInit(): void {
    // NOW UI Kit balast
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.getSupplyPlaces();

    this.lang = this.translate.currentLang;
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );
    this.auth.$user
      .pipe(take(1))
      .toPromise()
      .then((r) => {
        this.user = r;
      });
  }

  async getSupplyPlaces() {
    this.$supplyPoints = await this.supplyService.getSupplyPlaces();
    this.$supplyPoints.subscribe((d) => {
      this.pointChecked = [];
      for (let i = 0; i < d.length; ++i) {
        this.pointChecked.push(false);
      }
    });
  }

  deleteSupplyPlace(supplyPoint: string) {
    this.onPointClick('', -1);
    this.supplyService.deleteSupplyPlace(supplyPoint);
  }

  onPointClick(supplyPoint: string, index: number) {
    this.pointChecked[index] = !this.pointChecked[index];
    if (this.pointChecked[index]) {
      this.selectedPoint = supplyPoint;
    } else {
      this.selectedPoint = '';
    }
    for (let i = 0; i < this.pointChecked.length; ++i) {
      if (i !== index) {
        this.pointChecked[i] = false;
      }
    }
  }

  isTrue = (el: boolean) => el === true;

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.langSub.unsubscribe();
  }
}
