import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { Adress } from 'app/models/adress.model';
import { SupplyPointService } from 'app/services/supply-place.service';
import { Observable, Subscription } from 'rxjs';
import { District } from 'app/models/district.model';
import { tap, take, map } from 'rxjs/operators';
import { TariffService } from 'app/services/tariff.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-set',
  templateUrl: './set.component.html',
  styleUrls: ['./set.component.scss'],
})
export class SetComponent implements OnInit, OnDestroy {
  focus = new Array<boolean>(5);
  timeout: any = null;
  isNewPoint: boolean;

  supplyPoint: SupplyPlace;

  circuitBreakers: [];
  circuitBreakerList = [];
  circuitBreaker: any;
  circuitBreakersDropdownSettings = {};
  circuitBreakerSub: Subscription;

  lang: string;
  langSub: Subscription;

  distributor: Observable<District[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sps: SupplyPointService,
    private tariffService: TariffService,
    private translate: TranslateService
  ) {}
  // nastaveni odberneho mista

  ngOnInit() {
    this.lang = this.translate.currentLang || 'en';
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.route.queryParams.subscribe((d: Params) => {
      d.new ? (this.isNewPoint = true) : (this.isNewPoint = false);
      if (d.supplyPoint) {
        this.getSupplyPlace(d.supplyPoint);
      } else {
        this.supplyPoint = new SupplyPlace('', new Adress('', '', '', '', ''));
        this.getDistributor();
      }
    });

    this.circuitBreaker = [];

    this.circuitBreakerSub = this.tariffService.circuitBreakers
      .pipe(
        map((circuitBreakers) => {
          const cbList = [];
          circuitBreakers.sort((a, b) => {
            if (+a.id < +b.id) {
              return -1;
            }
            if (+a.id > +b.id) {
              return 1;
            }
            return 0;
          });

          for (const circuitBreaker of circuitBreakers) {
            cbList.push({
              id: circuitBreaker.id,
              itemName: circuitBreaker.description[this.lang],
            });
          }

          return cbList;
        })
      )
      .subscribe((res) => {
        this.circuitBreakerList = res;
      });

    this.circuitBreakersDropdownSettings = {
      singleSelection: true,
      text: this.translate.instant('police.setTariff.circuitBreaker'),
      searchPlaceholderText: this.translate.instant('police.tariffs.search'),
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: 'dropdownSelect',
      lazyLoading: true,
      disabled: false,
    };
  }

  async getSupplyPlace(uid: string) {
    this.supplyPoint = await (await this.sps.getSupplyPlace(uid))
      .pipe(take(1))
      .toPromise();
    this.supplyPoint.uid = uid;
    this.tariffService.circuitBreakers
      .pipe(
        map((d) => {
          // console.log(d, this.supplyPoint);
          for (const breaker of d) {
            if (+breaker.id === +this.supplyPoint.circuitBreaker) {
              return breaker;
            }
          }
          return {
            id: -1,
            description: {
              [this.lang]: 'Something went wrong. Choose circuit breaker again',
            },
          };
        })
      )
      .subscribe((r) => {
        // console.log(r);
        this.circuitBreaker.push({
          id: r.id,
          itemName: r.description[this.lang],
        });
      });
    // console.log(this.supplyPoint);
    this.getDistributor();
  }

  setCircuitBreaker(id: string) {
    this.supplyPoint.circuitBreaker = +id;
    // console.log(this.supplyPoint);
  }

  onKeySearch(event: any) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (event.keyCode !== 13) {
        this.getDistributor();
      }
    }, 1000);
  }

  getDistributor() {
    this.distributor = this.sps
      .getDistributorByPostalCode(this.supplyPoint.adress.postalCode)
      .pipe(
        tap((d) => {
          // console.log(d);
          if (d.length > 1) {
            console.log(
              'More than one distributor found. Please select district.'
            );
          }
        })
      );
  }

  async onSubmit(form: NgForm) {
    const dist = await this.distributor.pipe(take(1)).toPromise();
    this.supplyPoint.distributor = dist[0].distributor;
    if (this.isNewPoint) {
      this.sps
        .addSupplyPlace(this.supplyPoint)
        .then(() => this.router.navigate(['/police/dashboard']));
    } else {
      this.sps
        .updateSupplyPlace(this.supplyPoint.uid, this.supplyPoint)
        .then(() => this.router.navigate(['/police/dashboard']));
    }
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.langSub.unsubscribe();
    this.circuitBreakerSub.unsubscribe();
  }
}
