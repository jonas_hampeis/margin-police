import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PoliceComponent } from './police.component';
import { TariffComponent } from './tariff/tariff.component';
import { AuthGuard } from '../auth/auth.guard';
import { TariffCompareComponent } from './tariff-compare/tariff-compare.component';
import { TrackLessComponent } from './track/track.component';
import { UploadComponent } from '../shared/upload/upload.component';
import { SetTariffComponent } from './set-tariff/set-tariff.component';
import { IndicativeEstimateComponent } from './indicative-estimate/indicative-estimate.component';
import { RecapitulationComponent } from './recapitulation/recapitulation.component';
import { SupplyPlacesComponent } from './supply-places/supply-places.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SetComponent } from './supply-places/set/set.component';
import { UploadDocsComponent } from './upload-docs/upload-docs.component';

const routes: Routes = [
  {
    path: 'police',
    component: PoliceComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: SupplyPlacesComponent },
      {
        path: 'tariff',
        component: TariffComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'set-tariff',
        component: SetTariffComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'track',
        component: TrackLessComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'tariff-compare',
        component: TariffCompareComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'recap',
        component: RecapitulationComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'dashboard',
        component: SupplyPlacesComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'dashboard/set-point',
        component: SetComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'upload',
        component: UploadComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'upload-docs',
        component: UploadDocsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'estimate',
        component: IndicativeEstimateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'settings',
        component: NotificationsComponent,
        canActivate: [AuthGuard],
      },
      // { path: '**', component: SupplyPlacesComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoliceRoutingModule {}
