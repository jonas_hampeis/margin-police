import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { User } from 'app/models/user.model';
import { AuthService } from 'app/services/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Tariff } from 'app/models/tariff.model';
import { SupplyPointService } from 'app/services/supply-place.service';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { take, map } from 'rxjs/operators';
import { TariffService } from 'app/services/tariff.service';
import { CircuitBreaker } from 'app/models/circuitBreaker.model';
import { Rate } from 'app/models/rate.model';

@Component({
  selector: 'app-recapitulation',
  templateUrl: './recapitulation.component.html',
  styleUrls: ['./recapitulation.component.scss'],
})
export class RecapitulationComponent implements OnInit, OnDestroy {
  paramsSub: Subscription;
  providerParam: string;
  tariffParam: string;
  tariff: Observable<Tariff>;
  user: User;
  supplyPointUid: string;
  supplyPoint: SupplyPlace;
  circuitBreaker: Observable<CircuitBreaker>;
  rate: Observable<Rate>;
  tariffUid: string;

  agreeToTerms: boolean;

  readList = [];
  confirmRead = ['contract', 'priceList', 'termsConditions'];
  readAll = false;

  lang: string;
  langSub: Subscription;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private db: AngularFirestore,
    private sps: SupplyPointService,
    private translate: TranslateService,
    private tfs: TariffService
  ) {}
  // zobrazeni nove smlouvy a souhrn udaju

  ngOnInit(): void {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.lang = this.translate.currentLang;
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    this.paramsSub = this.route.queryParams.subscribe((params: Params) => {
      this.providerParam = params.provider;
      this.tariffParam = params.tariff;
      this.supplyPointUid = params.supplyPoint;
      this.tariffUid = params.userTariff;
      this.getRate();
      this.getSupplyPlace();
    });
    this.getUserInfo();
    this.tariff = this.getProductInfo();
  }

  async getSupplyPlace() {
    this.supplyPoint = await (
      await this.sps.getSupplyPlace(this.supplyPointUid)
    )
      .pipe(take(1))
      .toPromise();

    this.getCircuitBreaker();
  }

  async getCircuitBreaker() {
    this.circuitBreaker = this.tfs.circuitBreakers.pipe(
      map((cbs) => {
        const cb = cbs.find((d) => +d.id === +this.supplyPoint.circuitBreaker);
        return cb;
      })
    );
  }

  async getRate() {
    const userTariff = await (
      await this.tfs.getUserTariff(this.supplyPointUid, this.tariffUid)
    )
      .pipe(take(1))
      .toPromise();

    this.rate = this.tfs.rates.pipe(
      map((rs) => {
        const r = rs.find((d) => +d.id === +userTariff.rate);
        return r;
      })
    );
  }

  async getUserInfo() {
    this.user = await this.auth.getUserInfo();
  }

  getProductInfo() {
    return this.db
      .doc<Tariff>(
        `providers/${this.providerParam}/tariffs/${this.tariffParam}`
      )
      .valueChanges();
  }

  addToRead(doc: string) {
    this.readList.push(doc);
    let read = 0;
    for (const i of this.confirmRead) {
      if (this.readList.includes(i)) {
        ++read;
      }
    }
    if (read === 3) {
      this.readAll = true;
    }
  }

  ngOnDestroy() {
    if (this.paramsSub) {
      this.paramsSub.unsubscribe();
    }
    if (this.langSub) {
      this.langSub.unsubscribe();
    }
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
