import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { User } from 'app/models/user.model';
import { Observable, of, Subscription } from 'rxjs';
import { UserTariff } from 'app/models/userTariff.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TariffService } from 'app/services/tariff.service';
import { SupplyPointService } from 'app/services/supply-place.service';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { map, take } from 'rxjs/operators';

@Component({
  selector: 'app-police-landing',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.scss'],
})
export class TariffComponent implements OnInit, OnDestroy {
  isLoggedIn: Promise<boolean>;
  user: User;
  $userTariff: Observable<UserTariff>;
  paramsSub: Subscription;

  supplyPointUid: string;
  tariffUid: string;

  isNewTariff = false;
  isEmptyTariff: boolean;

  lang: string;
  langSub: Subscription;

  createdTariff = false;

  energySelection = {
    electricity: {
      name: 'Electricity',
      value: 'electricity',
      checked: false,
      icon: 'fa-bolt',
    },
    gas: {
      name: 'Gas',
      value: 'gas',
      checked: false,
      icon: 'fa-fire',
    },
  };
  selectedEnergy: string;
  supplyPointName: Observable<string>;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private tfs: TariffService,
    private supplyService: SupplyPointService,
    private router: Router,
    private translate: TranslateService
  ) {}
  // zajistuje prvni pruchod aplikaci
  // a nasledne spravu tarifu

  ngOnInit() {
    this.paramsSub = this.route.queryParams.subscribe((p: Params) => {
      this.supplyPointUid = p.supplyPoint;
      this.tariffUid = p.tariff;
      if (p.newUser === 'true' || p.newTariff === 'true') {
        this.isNewTariff = true;
        this.$userTariff = of(null);
        this.isEmptyTariff = true;

        if (p.newTariff === 'true' && p.supplyPoint) {
          this.createNewTariff(p.supplyPoint);
        } else {
          this.createNewTariff();
        }
      }
      if (p.isEmptyTariff === 'true') {
        this.isEmptyTariff = true;
      } else {
        this.getTariff(p.supplyPoint, p.tariff);
      }
    });
    this.user = this.auth.user;

    this.isLoggedIn = this.auth.isLoggedIn();

    this.lang = this.translate.currentLang;
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    this.getEnergyAndSupplyPoint();

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  async getEnergyAndSupplyPoint() {
    this.selectedEnergy = await (
      await this.tfs.getUserTariff(this.supplyPointUid, this.tariffUid)
    )
      .pipe(
        map((d) => {
          if (d) {
            return d.energy;
          } else {
            return null;
          }
        }),
        take(1)
      )
      .toPromise();

    this.supplyPointName = (
      await this.supplyService.getSupplyPlace(this.supplyPointUid)
    ).pipe(map((d) => d.name));
  }

  onEnergyClick(key: string) {
    this.energySelection[key].checked = !this.energySelection[key].checked;
    this.tfs.setEnergy(this.supplyPointUid, this.tariffUid, key);
    if (key === 'gas') {
      this.energySelection.electricity.checked = false;
    } else {
      this.energySelection.gas.checked = false;
    }
    this.selectedEnergy = key;
  }

  deleteTariff(supplyPoint: string, tariff: string) {
    this.tfs.deleteTariff(supplyPoint, tariff);
    this.router.navigate(['/police']);
  }

  async createNewTariff(supplyPoint?: string) {
    if (!supplyPoint) {
      this.supplyPointUid = await this.supplyService.addBlankSupplyPlace();
    } else {
      this.supplyPointUid = supplyPoint;
    }
    this.tariffUid = (
      await this.tfs.addBlankTariff(this.supplyPointUid)
    ).tariff;
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        supplyPoint: this.supplyPointUid,
        tariff: this.tariffUid,
        isEmptyTariff: true,
      },
    });
  }

  async getTariff(supplyPoint: string, tariff: string) {
    this.$userTariff = await this.tfs.getUserTariff(supplyPoint, tariff);
  }
  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.paramsSub.unsubscribe();
    this.langSub.unsubscribe();
    console.log(this.createdTariff, this.isNewTariff, this.selectedEnergy);
    if ((!this.createdTariff && this.isNewTariff) || !this.selectedEnergy) {
      this.tfs.deleteTariff(this.supplyPointUid, this.tariffUid);
    }
  }
}
