import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrackingService } from 'app/services/tracking.service';
import { Observable, Subscription, of } from 'rxjs';
import { FirestoreMeterReading } from 'app/models/firestoreMeterReading.model';
import { Comparison } from 'app/models/comparison.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { TariffService } from 'app/services/tariff.service';
import { UserTariff } from 'app/models/userTariff.model';
import { PeriodSummary } from 'app/models/periodSummary.model';
import { CalculatePrice } from 'app/models/calculatePrice.model';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { SupplyPointService } from 'app/services/supply-place.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-track-less',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
})
export class TrackLessComponent implements OnInit, OnDestroy {
  focus: boolean;
  focus1: boolean;
  addEntry = false;
  deductionDate: Date;
  deductedValue: number;
  deductedValueLow: number;
  trackingEntries: Observable<FirestoreMeterReading[]>;
  periodSummary: Observable<{
    periodSummary: PeriodSummary;
    calcData: CalculatePrice;
  }>;
  betterTariff: boolean;
  numberOfBetterTariffs = 0;
  comparisons: Comparison[];
  public isCollapsed = true;
  params: string;
  paramsSub: Subscription;
  langSub: Subscription;
  lang: string;
  title = false;
  tariffUid: string;
  supplyPointUid: string;
  tariff: Observable<UserTariff>;
  revDataSub: Subscription;
  entryDate = [];

  unitFees: number;
  monthlyFees: number;
  feeSub: Subscription;
  haveFees: boolean;

  supplyPoint: Promise<SupplyPlace>;
  entriesSub: Subscription;

  constructor(
    private ts: TrackingService,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private tfs: TariffService,
    private sps: SupplyPointService
  ) {}
  // komponenta pro sledovani spotreby na tarifu

  ngOnInit() {
    this.paramsSub = this.route.queryParams.subscribe((params: Params) => {
      this.params = params.version;
      this.tariffUid = params.tariff;
      this.supplyPointUid = params.supplyPoint;
      if (this.tariffUid && this.supplyPointUid) {
        this.getEntriesFromDB();
        this.getTariff();
        this.loadReviewData();
        this.getSupplyPlace();
      } else {
        this.router.navigate(['/police']);
      }
    });

    this.lang = this.translate.currentLang;
    this.langSub = this.translate.onLangChange.subscribe(
      (lang: LangChangeEvent) => {
        this.lang = lang.lang;
      }
    );

    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  async getSupplyPlace() {
    this.supplyPoint = (await this.sps.getSupplyPlace(this.supplyPointUid))
      .pipe(take(1))
      .toPromise();
  }

  async getEntriesFromDB() {
    this.trackingEntries = await this.ts.getMeterReadings(
      this.supplyPointUid,
      this.tariffUid
    );
    this.entriesSub = this.trackingEntries.subscribe((d) => {
      for (const entry of d) {
        this.entryDate.push(entry.deductionDate.toDate());
      }
    });
  }

  async getTariff() {
    this.tariff = await this.tfs.getUserTariff(
      this.supplyPointUid,
      this.tariffUid
    );
  }

  async loadReviewData() {
    this.periodSummary = await this.ts.getPeriodSummary(
      this.supplyPointUid,
      this.tariffUid
    );
    this.feeSub = this.periodSummary.subscribe((d) => {
      console.log(d);
      this.monthlyFees = d.periodSummary.totalMonthlyFees;
      this.unitFees = d.periodSummary.totalUnitFees / 1000;
      if (this.monthlyFees) {
        this.compareTariffs();
      }
    });
  }

  async compareTariffs() {
    const revData = await this.periodSummary.pipe(take(1)).toPromise();

    if (revData) {
      this.comparisons = (
        await this.ts.compareTariffs(revData.calcData)
      ).comparisons;
      for (const estimate of this.comparisons) {
        if (estimate.isBetter) {
          this.betterTariff = true;
          this.numberOfBetterTariffs++;
        }
      }
    }
  }

  // skvele pojmenovani, za to se musim pochvalit
  // ne, proste jsem to odnekud zkopiroval a neobtezoval se pojemnovat to nejak rozumne
  // filtruje data v datepickeru, neni mozne mit dva odecty se stejnym datem
  public myFilter = (d: Date): boolean => {
    let isPossible = true;
    for (const date of this.entryDate) {
      if (+date === +d) {
        isPossible = false;
        break;
      }
    }
    return isPossible;
  };

  onSubmit() {
    if (this.deductedValue === null || this.deductedValue === undefined) {
      return;
    }
    this.addEntry = false;
    this.ts.setMeterReading(
      this.deductionDate,
      this.deductedValue,
      this.deductedValueLow || null,
      this.supplyPointUid,
      this.tariffUid
    );
  }

  onButtonClick() {
    this.addEntry = !this.addEntry;
  }

  onBetterTariffClick() {
    this.ts.comparisons = this.comparisons;
    this.router.navigate(['/police/tariff-compare'], {
      queryParams: {
        tariff: this.tariffUid,
        supplyPoint: this.supplyPointUid,
      },
    });
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.paramsSub.unsubscribe();
    this.langSub.unsubscribe();
    if (this.revDataSub) {
      this.revDataSub.unsubscribe();
    }
    if (this.feeSub) {
      this.feeSub.unsubscribe();
    }
    if (this.entriesSub) {
      this.entriesSub.unsubscribe();
    }
  }
}
