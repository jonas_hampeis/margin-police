import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/filter';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-police',
  templateUrl: './police.component.html',
  styleUrls: ['./police.component.scss'],
})
export class PoliceComponent implements OnInit {
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('en');
  }
  ngOnInit() {}
}
