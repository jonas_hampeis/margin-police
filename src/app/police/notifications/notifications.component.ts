import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'app/services/auth.service';
import { take } from 'rxjs/operators';
import { User } from 'app/models/user.model';
import { Settings } from 'app/models/settings.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit, OnDestroy {
  focus = new Array<boolean>(3);

  beforeEndContract: boolean;
  beforeRenewal: boolean;
  whenCanSave: boolean;
  canSaveAmount: number;
  whenCostUp: boolean;
  toRead: boolean;
  toReadEvery: number;
  consumptionUp: boolean;
  consumptionUpBy: number;

  userSettings: Settings;

  constructor(
    private auth: AuthService,
    private db: AngularFirestore,
    private router: Router
  ) {}
  // nastaveni notifikaci
  // pouze se uklada do db

  ngOnInit(): void {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.getUserSettings();
  }

  async getUserSettings() {
    this.userSettings = await this.auth.getUserSettings();
    // console.log(this.userSettings);
  }

  async onSubmit(form: NgForm) {
    // console.log(form);
    const uid = (await this.auth.$user.pipe(take(1)).toPromise()).uid;
    await this.db.doc(`users/${uid}`).set(
      {
        settings: {
          lang: this.userSettings.lang,
          notifications: { ...this.userSettings.notifications },
        },
      },
      { merge: true }
    );
    this.router.navigate(['/police/dashboard']);
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
