import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { AuthService } from 'app/services/auth.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-upload-docs',
  templateUrl: './upload-docs.component.html',
  styleUrls: ['./upload-docs.component.scss'],
})
export class UploadDocsComponent implements OnInit, OnDestroy {
  tariffUid: string;
  supplyPointUid: string;
  paramsSub: Subscription;

  constructor(private auth: AuthService, private route: ActivatedRoute) {}
  // testovaci komponenta pro nahrani dokumentu a ulozeni do db

  ngOnInit(): void {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.paramsSub = this.route.queryParams.subscribe((p: Params) => {
      this.supplyPointUid = p.supplyPoint;
      this.tariffUid = p.tariff;
    });
  }

  async fileChange(event: any, type: string) {
    const user = await this.auth.getUserId();
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      const storageRef = firebase.storage().ref();
      const docRef = storageRef.child(
        `${user}/${this.supplyPointUid}/${this.tariffUid}/${type}/${file.name}`
      );
      docRef.put(file).then(() => console.log('uploaded file'));
    }
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    this.paramsSub.unsubscribe();
  }
}
