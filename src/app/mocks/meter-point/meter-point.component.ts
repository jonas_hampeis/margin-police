import { Component, OnInit, OnDestroy } from '@angular/core';
@Component({
  selector: 'app-indicative-estimate',
  templateUrl: './meter-point.component.html',
  styleUrls: ['./meter-point.component.scss'],
})
export class MeterPointComponent implements OnInit, OnDestroy {
  constructor() {}

  ngOnInit() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
