import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'app/services/auth.service';
import { User } from 'app/models/user.model';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  focus = new Array<boolean>(9);
  birthDate: Date;

  user: User;

  params: string;
  paramsSub: Subscription;

  constructor(
    private db: AngularFirestore,
    private auth: AuthService,
    private route: ActivatedRoute
  ) {}
  // sprava profilu uzivatele

  ngOnInit(): void {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    this.paramsSub = this.route.queryParams.subscribe((params: Params) => {
      this.params = params.fromNewContract;
    });

    this.getUserInfo();
  }

  async getUserInfo() {
    this.user = await this.auth.getUserInfo();
    this.birthDate = this.user.birthDate.toDate();
  }

  // async onNameChange(userName: string) {
  //   if (this.nameDuplicate) {
  //     this.nameDuplicate = null;
  //   }
  //   this.db
  //     .collection('users', (ref) => ref.where('userName', '==', userName))
  //     .valueChanges()
  //     .subscribe((res) => {
  //       if (res.length > 0) {
  //         this.nameDuplicate = this.auth.handleError(
  //           'auth/user-name-already-in-use'
  //         );
  //       }
  //     });
  // }

  onSubmit(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === undefined) {
        form.value[key] = null;
      }
    }
    this.db.doc(`users/${this.user.uid}`).update(form.value);
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
}
