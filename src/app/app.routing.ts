import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './examples/landing/landing.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { MeterPointComponent } from './mocks/meter-point/meter-point.component';

const routes: Routes = [
  {
    path: 'examples/landing',
    component: LandingComponent,
    canActivate: [AuthGuard],
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  {
    path: 'meter',
    component: MeterPointComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'police',
    loadChildren: () =>
      import('./police/police.module').then((m) => m.PoliceModule),
  },
  {
    path: 'light',
    loadChildren: () =>
      import('./light/light.module').then((m) => m.LightModule),
  },
  { path: '', redirectTo: 'examples/landing', pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: false,
    }),
  ],
  exports: [],
})
export class AppRoutingModule {}
