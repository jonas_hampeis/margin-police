export class MeterReading {
  constructor(
    public deductionDate: Date,
    public value: number,
    public valueLow?: number
  ) {}
}
