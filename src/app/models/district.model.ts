export class District {
  id: number;
  constructor(
    public country: string,
    public distributor: string,
    public district: string,
    public postalCode: Array<number>
  ) {}
}
