export class Rate {
  id: number;
  constructor(
    public rate: string,
    public isTwoTariff: boolean,
    public description: object
  ) {}
}
