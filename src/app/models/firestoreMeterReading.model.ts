import { Timestamp } from '@firebase/firestore-types';
export class FirestoreMeterReading {
  constructor(
    public deductionDate: Timestamp,
    public value: number,
    public valueLow?: number
  ) {}
}
