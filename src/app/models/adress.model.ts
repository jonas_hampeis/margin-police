export class Adress {
  constructor(
    public postalCode: string,
    public city: string,
    public street: string,
    public supplyPointNumber: string,
    public EAN: string
  ) {}
}
