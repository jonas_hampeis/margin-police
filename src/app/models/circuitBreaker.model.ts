export class CircuitBreaker {
  id: number;
  constructor(
    public phase: number,
    public circuitBreakerFrom: number,
    public circuitBreakerTo: number,
    public calculation: string,
    public rate: string,
    public description: object
  ) {}
}
