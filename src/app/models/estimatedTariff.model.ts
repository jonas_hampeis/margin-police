import { PeriodSummary } from './periodSummary.model';
import { Provider } from './provider.model';
import { Rate } from './rate.model';

export class EstimatedTariff {
  constructor(
    public place: 'household' | 'business',
    public objectType: 'flat' | 'house' | 'weekendCottage',
    public people: number,
    public usages?: Array<string>,
    public provider?: Provider,
    public deposit?: number,
    public periodSummary?: PeriodSummary,
    public rate?: Rate,
    public energy?: string,
    public from = 'estimate'
  ) {}
}
