import { PeriodSummary } from './periodSummary.model';
import { Advantage } from './advantage.model';
import * as firebase from 'firebase/app';
import 'firebase/firestore';

export class Tariff {
  constructor(
    public uid: string,
    public name: string,
    public energy: string,
    // jednotkovou cenu uz demo nepouziva, vypocet se provadi pres cloud functions
    public unitPrice: number,
    public validFrom: firebase.firestore.Timestamp,
    public validTo: firebase.firestore.Timestamp,
    public contractDurationMonths?: number,
    public fixedPriceDurationMonths?: number,
    public advantages?: Advantage,
    public disadvantages?: Advantage,
    public periodSummary?: PeriodSummary,
    public providerUid?: string,
    public currency?: string,
    public unit?: string
  ) {}
}
