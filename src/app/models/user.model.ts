import { UserTariff } from './userTariff.model';
import { Settings } from './settings.model';

export class User {
  constructor(
    public uid: string,
    public email: string,
    public userName: string,
    public firstName?: string,
    public lastName?: string,
    public phoneNumber?: string,
    public settings?: Settings,
    public tariff?: Array<UserTariff>,
    public contractTariff?: Array<UserTariff>,
    public birthDate?: firebase.firestore.Timestamp,
    public street?: string,
    public city?: string,
    public postalCode?: string,
    public customerNumber?: string,
    public accountNumber?: string
  ) {}
}
