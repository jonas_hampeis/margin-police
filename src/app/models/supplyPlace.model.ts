import { Adress } from './adress.model';
import { Observable } from 'rxjs';
import { UserTariff } from './userTariff.model';

export class SupplyPlace {
  constructor(
    public name: string,
    public adress: Adress,
    public uid?: string,
    public tariffs?: Observable<UserTariff[]>,
    public distributor?: string,
    public circuitBreaker?: number
  ) {}
}
