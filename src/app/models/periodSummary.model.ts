export class PeriodSummary {
  constructor(
    public from: 'billing' | 'tracking' | 'estimate',
    public measuredConsumption: number,
    public measuredCost: number,
    public annualConsumption: number,
    public annualCost: number,
    public isTwoTariff: boolean,
    public measuredConsumptionLow?: number,
    public annualConsumptionLow?: number,
    public measuredPeriod?: number,
    public totalMonthlyFees?: number,
    public totalUnitFees?: number
  ) {}
  // measured... je hodnota za sledovane obdobi
  // annual... je hodnota za rok
}
