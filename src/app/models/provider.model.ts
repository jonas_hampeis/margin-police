import { Tariff } from './tariff.model';

export class Provider {
  constructor(
    public uid: string,
    public name: string,
    public logo?: string,
    public rating?: number,
    public providedEnergies?: Array<string>,
    public tariffs?: Array<Tariff>
  ) {}
}
