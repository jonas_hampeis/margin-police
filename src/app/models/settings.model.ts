import { Notifications } from './notifications.model';

export class Settings {
  constructor(
    public lang: string = 'en',
    public notifications: Notifications = new Notifications()
  ) {}
}
