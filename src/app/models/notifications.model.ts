export class Notifications {
  constructor(
    public contractBeforeEnd: boolean = false,
    public contractBeforeRenewal: boolean = false,
    public canSave: boolean = false,
    public canSaveAmount: number = 2500,
    public costHigher: boolean = false,
    public remindToRead: boolean = false,
    public remindToReadEvery: number = 2,
    public consumptionHigher: boolean = false,
    public consumptionHigherByPercent: number = 20
  ) {}
}
