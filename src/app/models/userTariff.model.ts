import { Timestamp } from '@google-cloud/firestore';
import { PeriodSummary } from './periodSummary.model';

export class UserTariff {
  constructor(
    public uid: string,
    public name: string,
    public unitPrice: number,
    public validFrom: Timestamp,
    public validTo: Timestamp,
    public place: string,
    public rate: number,
    public circuitBreaker: number,
    public energy: string,
    public accountingFrom?: Timestamp,
    public accountingTo?: Timestamp,
    public contractDuration?: string,
    public customerNumber?: string,
    public definiteDuration?: Date,
    public autoContractExtension?: boolean,
    public periodSummary?: PeriodSummary,
    public providerUid?: string,
    public currency?: string,
    public unit?: string,
    public from = 'billing'
  ) {}
}
