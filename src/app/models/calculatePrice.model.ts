export class CalculatePrice {
  constructor(
    public isTariffComparison: boolean,
    public consumption: number,
    public provider?: string,
    public tariff?: string,
    public distributor?: string,
    public rateId?: number,
    public circuitBreakerId?: number,
    public consumptionLow?: number,
    public phases?: number,
    public circuitBreaker?: number,
    public period?: number
  ) {}
}
