import { Tariff } from './tariff.model';
import { Provider } from './provider.model';

export class Comparison {
  constructor(
    public estimatedCost: number,
    public tariff: Tariff,
    public isBetter: boolean,
    public totalCost?: number,
    public provider?: Provider,
    public savedAmount?: number
  ) {}
}
