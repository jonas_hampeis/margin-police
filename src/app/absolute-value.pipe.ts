import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'absoluteValue',
})
// pipe na vypocet absolutni hodnoty
// pouziva se pri srovnani produktu
export class AbsoluteValuePipe implements PipeTransform {
  transform(value: number): number {
    return Math.abs(value);
  }
}
