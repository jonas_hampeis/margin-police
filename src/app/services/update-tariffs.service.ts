import { Injectable } from '@angular/core';
import { Provider } from 'app/models/provider.model';
import { AngularFirestore } from '@angular/fire/firestore';

export class TariffRaw {
  constructor(
    public name: string,
    public validFrom: number,
    public validTo: number,
    public contractDurationMonths: string,
    public fixedPriceDurationMonths: string,
    public advantages: Object,
    public disadvantages: Object,
    public unitPrice?: string
  ) {}
}

export class ProviderRaw {
  constructor(
    public name: string,
    public logo: string,
    public rating: number,
    public tariffs: Array<TariffRaw>
  ) {}
}

@Injectable({
  providedIn: 'root',
})
export class UpdateTariffsService {
  providers: Provider[];

  // slouzi pro nahravani dodavatelu a tarifu z excelu AnalyzaElProduktu do firebase db
  // nez se psat s takovymahle vylomeninama, je daleko jednodussi na to mit node skript
  // na to jsem ale prisel az pozdeji
  constructor(private db: AngularFirestore) {}

  updateTariff(provider: ProviderRaw) {
    const validFrom = new Date(provider.tariffs[0].validFrom * 1000);
    const validTo = new Date(provider.tariffs[0].validTo * 1000);
    const unitPrice = +provider.tariffs[0].unitPrice.replace(',', '.');
    const contractDurationMonths =
      provider.tariffs[0].contractDurationMonths === 'doba neurčitá'
        ? -1
        : +provider.tariffs[0].contractDurationMonths.slice(0, 2);
    let fixedPriceDurationMonths;
    if (provider.tariffs[0].fixedPriceDurationMonths === 'ne') {
      fixedPriceDurationMonths = 0;
    } else if (
      provider.tariffs[0].fixedPriceDurationMonths.slice(0, 2) === 'do'
    ) {
      const dateText = provider.tariffs[0].fixedPriceDurationMonths.slice(
        3,
        provider.tariffs[0].fixedPriceDurationMonths.length
      );
      const date = new Date(dateText);
      // console.log(date);
      fixedPriceDurationMonths = Math.floor(
        (date.getMilliseconds() - Date.now()) / 2592000000
      );
    } else {
      fixedPriceDurationMonths = +provider.tariffs[0].fixedPriceDurationMonths.slice(
        0,
        2
      );
    }
    const tariff = {
      uid: provider.tariffs[0].name,
      name: provider.tariffs[0].name,
      energy: 'electricity',
      unitPrice: unitPrice,
      validFrom: validFrom,
      validTo: validTo,
      contractDurationMonths: contractDurationMonths,
      fixedPriceDurationMonths: fixedPriceDurationMonths,
      advantages: provider.tariffs[0].advantages,
      disadvantages: provider.tariffs[0].disadvantages,
      providerUid: provider.name,
    };
    // console.log(tariff);
    this.db.doc(`/providers/${provider.name}`).set({
      name: provider.name,
      logo: provider.logo,
      rating: provider.rating,
      providedEnergies: ['electricity'],
    });
    this.db
      .doc(`/providers/${provider.name}/tariffs/${provider.tariffs[0].name}`)
      .set({
        name: tariff.name,
        uid: tariff.uid,
        energy: tariff.energy,
        unitPrice: tariff.unitPrice,
        validFrom: tariff.validFrom,
        validTo: tariff.validTo,
        contractDurationMonths: tariff.contractDurationMonths,
        fixedPriceDurationMonths: tariff.fixedPriceDurationMonths,
        advantages: tariff.advantages,
        disadvantages: tariff.disadvantages,
        providerUid: tariff.providerUid,
        unit: 'kWh',
        currency: 'CZK',
      });
  }
}
