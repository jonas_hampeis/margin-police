import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'app/models/user.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, first, map, take } from 'rxjs/operators';
import { auth } from 'firebase/app';
import { UserTariff } from 'app/models/userTariff.model';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { Settings } from 'app/models/settings.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  $user: Observable<User>;
  user: User;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router
  ) {
    // pokud potrebujeme nejaka uzivatelska data, mela by byt v tehle promenne
    this.$user = this.afAuth.authState.pipe(
      switchMap((user) => {
        // Logged in
        if (user) {
          return this.db
            .doc<User>(`users/${user.uid}`)
            .snapshotChanges()
            .pipe(
              map((userRes) => {
                let newUser: User;
                if (userRes.payload.data()) {
                  newUser = new User(
                    userRes.payload.id,
                    userRes.payload.data().email,
                    userRes.payload.data().userName,
                    userRes.payload.data().firstName,
                    userRes.payload.data().lastName,
                    userRes.payload.data().phoneNumber,
                    userRes.payload.data().settings,
                    null,
                    null,
                    userRes.payload.data().birthDate,
                    userRes.payload.data().street,
                    userRes.payload.data().city,
                    userRes.payload.data().postalCode,
                    userRes.payload.data().customerNumber,
                    userRes.payload.data().accountNumber
                  );
                } else {
                  newUser = null;
                }
                return newUser;
              })
            );
        } else {
          // Logged out
          // this.$userTariff = of(null);
          return of(null);
        }
      })
    );
    // this.getUserInfo().then((us) => (this.user = us));
  }

  // pokud potrebujeme jednou data o uzivateli
  async getUserInfo() {
    return await this.$user.pipe(take(1)).toPromise();
  }

  // pokud potrebujeme id uzivatele
  async getUserId() {
    const r = await this.$user.pipe(take(1)).toPromise();
    return r.uid;
  }

  async getUserSettings() {
    let userSettings = new Settings();
    await this.$user
      .pipe(take(1))
      .toPromise()
      .then((r: User) => {
        if (r.settings) {
          userSettings = r.settings;
        }
        // console.log(userSettings);
      });
    return userSettings;
  }

  // obsolete function
  async getUserTariff(uid: string) {
    let tariff: UserTariff[];
    await this.db
      .collection<UserTariff>(`/users/${uid}/userTariff`)
      .valueChanges()
      .pipe(take(1))
      .toPromise()
      .then((res) => {
        // console.log(res);
        tariff = res;
      });
    return tariff;
  }

  // odhlasen / prihlasen
  getUserState() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  // obcas sam nevim, co delam
  async isLoggedIn() {
    const user = await this.getUserState();
    if (user) {
      return true;
    } else {
      return false;
    }
  }

  // vyuziti firebase auth pro vytvoreni uzivatele pres email
  async registerEmail(
    email: string,
    password: string,
    userName: string,
    firstName: string,
    lastName: string,
    phoneNumber: string
  ) {
    const res = await this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then(async () => {
        const uid = (await this.afAuth.currentUser).uid;
        const user = new User(
          uid,
          email,
          userName,
          firstName,
          lastName,
          phoneNumber
        );
        this.createUser(user);
        this.router.navigate(['/police'], { queryParams: { newUser: true } });
        return null;
      })
      .catch(function (error) {
        return error.code;
      });
    this.getUserInfo().then((us) => (this.user = us));
    return this.handleError(res);
  }

  // firebase auth google/facebook/twitter prihlaseni
  async OAuthSignIn(prov: string) {
    let provider: auth.AuthProvider;
    switch (prov) {
      case 'google':
        provider = new auth.GoogleAuthProvider();
        break;
      case 'facebook':
        provider = new auth.FacebookAuthProvider();
        break;
      case 'twitter':
        provider = new auth.TwitterAuthProvider();
        break;
    }
    const credential = await this.afAuth.signInWithPopup(provider);
    const user = new User(
      credential.user.uid,
      credential.user.email,
      credential.user.displayName,
      '',
      '',
      credential.user.phoneNumber
    );
    let location = '';
    let queryParams = {};
    const userExists = await this.userExists(user.uid);
    if (!userExists) {
      location = '/tariff';
      queryParams = { newUser: true };
    }
    this.createUser(user);
    this.user = user;
    this.router.navigate(['/police' + location], { queryParams: queryParams });
  }

  async userExists(uid: string) {
    const user = await this.db
      .doc(`users/${uid}`)
      .get()
      .pipe(first())
      .toPromise();
    if (user.exists) {
      return true;
    } else {
      return false;
    }
  }

  async appleSignin() {}

  // vytvoreni uzivatele v db
  createUser(user: User) {
    this.db
      .collection('users')
      .doc(user.uid)
      .set(
        {
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          phoneNumber: user.phoneNumber,
          userName: user.userName,
        },
        { merge: true }
      )
      .catch(function (error) {
        console.error('Error writing document: ', error);
      });
  }

  // prihlaseni mailem
  async signInEmail(email: string, password: string) {
    const res = await this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then(async () => {
        await this.getUserInfo().then((us) => (this.user = us));
        if (this.user.tariff.length > 0) {
          this.router.navigate(['/police/track']);
        } else {
          this.router.navigate(['/police']);
        }
        return null;
      })
      .catch(function (error) {
        return error.code;
      });
    return this.handleError(res);
  }

  signOut() {
    this.user = null;
    this.afAuth.signOut().catch(function (error) {
      console.log(error.code);
    });
  }

  // premapovani chybovych hlasek na human readable format, ktery
  // je ulozeny v assets/i18n
  handleError(err: string) {
    if (err == null) {
      return null;
    }
    let errMessage = 'error.';
    switch (err) {
      case 'auth/user-not-found':
        errMessage += 'userNotFound';
        break;
      case 'auth/email-already-in-use':
        errMessage += 'emailInUse';
        break;
      case 'auth/user-name-already-in-use':
        errMessage += 'userNameInUse';
        break;
      case 'auth/wrong-password':
        errMessage += 'wrongPassword';
        break;
      default:
        console.log(err);
        errMessage += 'somethingWentWrong';
    }
    return errMessage;
  }
}
