import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Provider } from 'app/models/provider.model';
import { take } from 'rxjs/operators';
import { Tariff } from 'app/models/tariff.model';
import { AuthService } from './auth.service';
import { UserTariff } from 'app/models/userTariff.model';
import { PeriodSummary } from 'app/models/periodSummary.model';
import { Rate } from 'app/models/rate.model';
import { Observable } from 'rxjs';
import { CircuitBreaker } from 'app/models/circuitBreaker.model';
import { EstimatedTariff } from 'app/models/estimatedTariff.model';
import * as firebase from 'firebase/app';
import 'firebase/functions';

@Injectable({
  providedIn: 'root',
})
export class TariffService {
  rates: Observable<Rate[]>;
  circuitBreakers: Observable<CircuitBreaker[]>;

  constructor(private db: AngularFirestore, private auth: AuthService) {
    // dotahuje sazby a jistice z ciselniku
    this.rates = this.db
      .collection<Rate>('rates')
      .valueChanges({ idField: 'id' });
    this.circuitBreakers = this.db
      .collection<CircuitBreaker>('circuitBreakers')
      .valueChanges({ idField: 'id' });
  }

  // nacteni dodavatelu pro potreby dropdownu pri vyberu tarifu
  getProviders(providedEnergy: string) {
    providedEnergy = 'electricity';
    return this.db
      .collection<Provider>('providers', (ref) =>
        ref.where('providedEnergies', 'array-contains', providedEnergy)
      )
      .valueChanges({ idField: 'uid' })
      .pipe(take(1));
  }

  // a neco podobneho pro tarify
  getTariffs(provider: string, energy: string) {
    return this.db
      .doc(`providers/${provider}`)
      .collection<Tariff>('tariffs', (ref) => ref.where('energy', '==', energy))
      .valueChanges();
  }

  async getUserTariff(supplyPointUid: string, tariffUid: string) {
    const user = await this.auth.getUserId();
    // const user = 'w7BiNmilV3bX5ISUr6KdM4pOqxi1';
    return this.db
      .doc<UserTariff>(
        `users/${user}/supplyPoint/${supplyPointUid}/tariff/${tariffUid}`
      )
      .valueChanges();
  }

  async addTariff(tariff: UserTariff, supplyPoint: string) {
    const user = await this.auth.getUserId();

    this.db
      .doc(`users/${user}/supplyPoint/${supplyPoint}/tariff/${tariff.uid}`)
      .set({
        ...tariff,
      });
  }

  // pri vytvareni tarifu se nejprve vytvori prazdny tarif
  // do ktereho se potom bud doplni informace vyplnenim formulare
  // nebo se pridaji informace ze sledovani
  async addBlankTariff(supplyPoint: string) {
    const user = await this.auth.getUserId();
    // if (!supplyPoint) {
    //   console.log('no supply point')
    //   supplyPoint = await this.supplyService.addBlankSupplyPlace();
    // }
    const uid = this.db.createId();
    this.db.doc(`users/${user}/supplyPoint/${supplyPoint}/tariff/${uid}`).set({
      uid: uid,
      name: 'Unknown Tariff',
      periodSummary: { isTwoTariff: false },
    });
    return { supplyPoint: supplyPoint, tariff: uid };
  }

  async setTariff(tariffUid: string, tariff: UserTariff, supplyPoint: string) {
    const user = await this.auth.getUserId();
    this.db
      .doc<UserTariff>(
        `users/${user}/supplyPoint/${supplyPoint}/tariff/${tariffUid}`
      )
      .set(JSON.parse(JSON.stringify(tariff)), { merge: true });
    // i really dont like this, but its better than deconstructing
    // the whole object, since firebase doesnt take custom objects
  }

  // odhadnute hodnoty tarifu se ukladaji take do tarifu
  // ale nic je v teto appce nepouziva
  async setEstimatedTariff(
    tariffUid: string,
    tariff: EstimatedTariff,
    supplyPoint: string
  ) {
    const user = await this.auth.getUserId();
    this.db
      .doc<EstimatedTariff>(
        `users/${user}/supplyPoint/${supplyPoint}/tariff/${tariffUid}`
      )
      .set(JSON.parse(JSON.stringify(tariff)), { merge: true });
  }

  // firebase nijak jednoduse nepodporuje rekurzivni mazani dokumentu
  // takze se musi nejdriv smazat vsechny subkolekce
  // coz jsem si tady zkousel pres cloud functions, ktery to zvladaj trochu lip
  async deleteTariff(supplyPoint: string, tariffUid: string) {
    const user = await this.auth.getUserId();
    const deleteEntries = firebase.functions().httpsCallable('deleteEntries');
    deleteEntries({
      supplyPointUid: supplyPoint,
      tariffUid: tariffUid,
      userId: user,
    });
    this.db
      .doc(`users/${user}/supplyPoint/${supplyPoint}/tariff/${tariffUid}`)
      .delete();
  }

  async setPeriodSummary(
    revData: PeriodSummary,
    supplyPoint: string,
    tariff: string
  ) {
    const user = await this.auth.getUserId();
    this.db
      .doc(`/users/${user}/supplyPoint/${supplyPoint}/tariff/${tariff}`)
      .set({ periodSummary: { ...revData } }, { merge: true });
  }

  async setEnergy(supplyPoint: string, tariff: string, energy: string) {
    const user = await this.auth.getUserId();
    this.db
      .doc(`/users/${user}/supplyPoint/${supplyPoint}/tariff/${tariff}`)
      .set({ energy: energy }, { merge: true });
  }
}
