import { TestBed } from '@angular/core/testing';

import { UpdateTariffsService } from './update-tariffs.service';

describe('UpdateTariffsService', () => {
  let service: UpdateTariffsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateTariffsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
