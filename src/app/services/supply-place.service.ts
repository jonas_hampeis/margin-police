import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { SupplyPlace } from 'app/models/supplyPlace.model';
import { take, map, tap, first } from 'rxjs/operators';
import { UserTariff } from 'app/models/userTariff.model';
import { Adress } from 'app/models/adress.model';
import { District } from 'app/models/district.model';
import { TariffService } from './tariff.service';

@Injectable({
  providedIn: 'root',
})
export class SupplyPointService {
  constructor(
    private db: AngularFirestore,
    private auth: AuthService,
    private tfs: TariffService
  ) {}

  async getSupplyPlace(uid: string) {
    const user = await this.auth.getUserId();
    return this.db
      .doc<SupplyPlace>(`users/${user}/supplyPoint/${uid}`)
      .valueChanges();
  }

  // najde vsechna odberna mista a namapuje na ne jejich tarify
  // jak jste si jiste vsimli, jsem velice dobry ve vymysleni nazvu temp promennych
  async getSupplyPlaces() {
    const us = await this.auth.getUserId();
    const supplyPoints = this.db
      .collection<SupplyPlace>(`users/${us}/supplyPoint`)
      .valueChanges({ idField: 'uid' });
    return supplyPoints.pipe(
      map((d: SupplyPlace[]) => {
        for (const e of d) {
          e.tariffs = this.db
            .collection<UserTariff>(`users/${us}/supplyPoint/${e.uid}/tariff`)
            .valueChanges({ idField: 'uid' });
        }
        return d;
      })
    );
  }

  async addSupplyPlace(supplyPoint: SupplyPlace) {
    const user = await this.auth.getUserId();
    this.db
      .collection('users')
      .doc(user)
      .collection('supplyPoint')
      .add({
        name: supplyPoint.name,
        adress: { ...supplyPoint.adress },
        distributor: supplyPoint.distributor || 'unknown',
        circuitBreaker: supplyPoint.circuitBreaker || null,
      });
  }

  // pouziva se pro prvni pruchod aplikaci, kdy po uzivateli nechceme vsechny informace
  // ktere jsou pozdeji potreba
  async addBlankSupplyPlace() {
    console.log('adding blank supply point');
    const user = await this.auth.getUserId();
    const defaultDistributor = await this.db
      .collection('distributors', (ref) => ref.where('default', '==', true))
      .get()
      .toPromise();
    console.log(defaultDistributor.docs[0].id);
    const docRef = await this.db.collection(`users/${user}/supplyPoint`).add({
      name: 'Home',
      adress: { ...new Adress('', '', '', '', '') },
      distributor: defaultDistributor.docs[0].id,
    });
    return docRef.id;
  }

  async updateSupplyPlace(uid: string, supplyPoint: SupplyPlace) {
    const user = await this.auth.getUserId();
    this.db.doc<SupplyPlace>(`users/${user}/supplyPoint/${uid}`).set(
      {
        name: supplyPoint.name,
        adress: {
          postalCode: supplyPoint.adress.postalCode,
          city: supplyPoint.adress.city,
          street: supplyPoint.adress.street,
          supplyPointNumber: supplyPoint.adress.supplyPointNumber,
          EAN: supplyPoint.adress.EAN,
        },
        distributor: supplyPoint.distributor || 'unknown',
        circuitBreaker: supplyPoint.circuitBreaker || null,
      },
      { merge: true }
    );
  }

  async setCircuitBreaker(supplyPoint: string, circuitBreaker: number) {
    const user = await this.auth.getUserId();
    this.db.doc(`users/${user}/supplyPoint/${supplyPoint}`).set(
      {
        circuitBreaker: circuitBreaker,
      },
      { merge: true }
    );
  }

  async deleteSupplyPlace(supplyPoint: string) {
    const user = await this.auth.getUserId();
    const tariffs = this.db
      .collection(`users/${user}/supplyPoint/${supplyPoint}/tariff`)
      .get();
    const sub = tariffs.subscribe((d) => {
      for (const tariff of d.docs) {
        this.tfs.deleteTariff(supplyPoint, tariff.id);
      }
    });
    await this.db.doc(`users/${user}/supplyPoint/${supplyPoint}`).delete();
    sub.unsubscribe();
  }

  // vraci Distributora podle PSC z firestore
  getDistributorByPostalCode(postalCodeInput: string) {
    let postalCode = parseInt(postalCodeInput.replace(/\s+/g, ''), 10);
    if (isNaN(postalCode)) {
      postalCode = 0;
    }
    // console.log(postalCode);

    const distributor = this.db
      .collection<District>('postalCodes', (ref) =>
        ref.where('postalCode', 'array-contains', postalCode)
      )
      .valueChanges()
      .pipe(tap((d) => console.log(d)));

    return distributor;
  }
}
