import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MeterReading } from 'app/models/meterReading.model';
import { take, flatMap } from 'rxjs/operators';
import { FirestoreMeterReading } from 'app/models/firestoreMeterReading.model';
import { Tariff } from 'app/models/tariff.model';
import { TariffService } from './tariff.service';
import { Comparison } from 'app/models/comparison.model';
import { PeriodSummary } from 'app/models/periodSummary.model';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { CalculatePrice } from 'app/models/calculatePrice.model';
import { SupplyPointService } from './supply-place.service';

@Injectable({
  providedIn: 'root',
})
export class TrackingService {
  tariffs = new Array<Tariff>();
  comparisons: Comparison[];

  constructor(
    private auth: AuthService,
    private db: AngularFirestore,
    private tfs: TariffService,
    private sps: SupplyPointService
  ) {}

  async getMeterReadings(supplyPoint: string, tariff: string) {
    const us = await this.auth.getUserId();
    return this.db
      .collection<FirestoreMeterReading>(
        `users/${us}/supplyPoint/${supplyPoint}/tariff/${tariff}/entries`,
        (ref) => ref.orderBy('deductionDate')
      )
      .valueChanges();
  }

  // posbira data ze sledovani spotreby
  // a vrati shrnuti za obdobi
  // pokud existuje sledovani a je aktualnejsi nez vyuctovani, pouzije se to
  // pokud ne, pouzije se vyuctovani
  // calcData slouzi pro vypocet ceny
  async getPeriodSummary(supplyPoint: string, tariff: string) {
    // ach, me prvni boje s asynchronnimi funkcemi
    const userTariff = await (await this.tfs.getUserTariff(supplyPoint, tariff))
      .pipe(take(1))
      .toPromise();
    const userSupplyPoint = await (await this.sps.getSupplyPlace(supplyPoint))
      .pipe(take(1))
      .toPromise();
    const calcData = new CalculatePrice(
      false,
      0,
      userTariff.providerUid || null,
      userTariff.name,
      userSupplyPoint.distributor || null,
      +userTariff.rate || null,
      +userTariff.circuitBreaker || null,
      0
    );

    const entries = await this.getMeterReadings(supplyPoint, tariff);
    const reviewDataObs = entries.pipe(
      flatMap(async (d) => {
        let entriesExist = false;
        if (d.length > 1) {
          entriesExist = true;
        }
        if (
          (entriesExist &&
            userTariff.accountingTo &&
            d[d.length - 1].deductionDate.seconds <
              userTariff.accountingTo.seconds) ||
          !entriesExist
        ) {
          console.log('billing is newer than tracking entries');
          calcData.consumption = userTariff.periodSummary.annualConsumption;
          calcData.consumptionLow =
            userTariff.periodSummary.annualConsumptionLow;
          userTariff.periodSummary.from = 'billing';
          return {
            periodSummary: userTariff.periodSummary,
            calcData: calcData,
          };
        } else {
          console.log('tracking entries exist');

          const totalConsumption = d[d.length - 1].value - d[0].value;
          const totalConsumptionLow = d[d.length - 1].valueLow - d[0].valueLow;

          const period =
            (d[d.length - 1].deductionDate.seconds -
              d[0].deductionDate.seconds) /
            86400;

          calcData.consumption = totalConsumption;
          calcData.consumptionLow = totalConsumptionLow;
          calcData.period = period;

          // console.log(calcData);
          let totalCosts = null;
          if (userTariff.providerUid) {
            const calc = firebase.functions().httpsCallable('calculatePrice');
            const totalCostCall = await calc(calcData);
            totalCosts = totalCostCall.data;
          }

          // console.log(totalCosts);

          const annualConsumption = (totalConsumption / period) * 365;
          const annualConsumptionLow = (totalConsumptionLow / period) * 365;

          const periodSummary = new PeriodSummary(
            'tracking',
            totalConsumption,
            totalCosts?.totalCost || null,
            annualConsumption,
            totalCosts?.totalAnnualCost || null,
            userTariff.periodSummary.isTwoTariff,
            totalConsumptionLow,
            annualConsumptionLow,
            period,
            totalCosts?.totalMonthlyFees || null,
            totalCosts?.totalUnitFees || null
          );
          this.tfs.setPeriodSummary(periodSummary, supplyPoint, tariff);
          // console.log(periodSummary);
          return { periodSummary: periodSummary, calcData: calcData };
        }
      })
    );
    return reviewDataObs;
  }

  // porovnani tarifu vyuziva cloud functions
  // je to cele silene pomale
  async compareTariffs(
    calcData: CalculatePrice
  ): Promise<{ comparisons: Comparison[]; defaults: boolean }> {
    calcData.isTariffComparison = true;

    let defaults = false;
    if (
      !calcData.circuitBreakerId ||
      !calcData.rateId ||
      !calcData.provider ||
      !calcData.distributor ||
      !calcData.tariff ||
      calcData.tariff === 'Unknown Tariff'
    ) {
      defaults = true;
    }

    const calc = firebase.functions().httpsCallable('calculatePrice');
    const comparisonsCall = await calc(calcData);
    const comparisons = comparisonsCall.data;

    if (!Array.isArray(comparisons)) {
      console.log(comparisons);
      return comparisons;
    }

    // jeden z mych mnoha boju s firebase Timestampy
    for (const comparison of comparisons) {
      comparison.tariff.validTo = firebase.firestore.Timestamp.fromMillis(
        comparison.tariff.validTo._seconds * 1000
      );
      comparison.tariff.validFrom = firebase.firestore.Timestamp.fromMillis(
        comparison.tariff.validFrom._seconds * 1000
      );
    }
    const modelComparisons: { comparisons: Comparison[]; defaults: boolean } = {
      comparisons,
      defaults,
    };
    return modelComparisons;
  }

  async setMeterReading(
    date: Date,
    value: number,
    valueLow: number,
    supplyPoint: string,
    tariff: string
  ) {
    const user = await this.auth.getUserId();

    this.db
      .collection<MeterReading>(
        `/users/${user}/supplyPoint/${supplyPoint}/tariff/${tariff}/entries`
      )
      .add({
        deductionDate: date,
        value: value,
        valueLow: valueLow,
      });
  }
}
