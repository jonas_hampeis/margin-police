import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  focus: boolean;
  focus1: boolean;
  error: string;
  messageDeleter: Subscription;

  constructor(private auth: AuthService) {}

  async onSubmit(form: NgForm) {
    const res = await this.auth.signInEmail(
      form.value.email,
      form.value.password
    );
    this.error = res;
    this.messageDeleter = form.statusChanges.subscribe(() => {
      this.error = '';
    });
  }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  googleSignin() {
    this.auth.OAuthSignIn('google');
  }
  facebookSignin() {
    this.auth.OAuthSignIn('facebook');
  }
  twitterSignin() {
    this.auth.OAuthSignIn('twitter');
  }
  appleSignin() {
    this.auth.appleSignin();
  }

  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    if (this.messageDeleter) {
      this.messageDeleter.unsubscribe();
    }
  }
}
