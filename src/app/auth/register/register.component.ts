import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { NgForm } from '@angular/forms';
import { User } from 'app/models/user.model';
import { Subscription } from 'rxjs';
import 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  focus = new Array(6);
  formWorking: boolean;
  user: User;
  error: string;
  nameDuplicate: string;
  messageDeleter: Subscription;
  userName = '';

  data: Date = new Date();

  constructor(public auth: AuthService, private db: AngularFirestore) {}

  ngOnInit() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  googleSignin() {
    this.auth.OAuthSignIn('google');
  }
  facebookSignin() {
    this.auth.OAuthSignIn('facebook');
  }
  twitterSignin() {
    this.auth.OAuthSignIn('twitter');
  }
  appleSignin() {
    this.auth.appleSignin();
  }

  async onNameChange(userName: string) {
    if (this.nameDuplicate) {
      this.nameDuplicate = null;
    }
    this.db
      .collection('users', (ref) => ref.where('userName', '==', userName))
      .valueChanges()
      .subscribe((res) => {
        if (res.length > 0) {
          this.nameDuplicate = this.auth.handleError(
            'auth/user-name-already-in-use'
          );
        }
      });
  }

  async onSubmit(form: NgForm) {
    const res = await this.auth.registerEmail(
      form.value.email,
      form.value.password,
      form.value.userName,
      form.value.firstName,
      form.value.lastName,
      form.value.phoneNumber
    );
    this.error = res;
    this.messageDeleter = form.statusChanges.subscribe(() => {
      this.error = '';
    });
  }

  ngOnDestroy() {
    const navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
    if (this.messageDeleter) {
      this.messageDeleter.unsubscribe();
    }
  }
}
