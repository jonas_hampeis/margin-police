import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'app/services/auth.service';
import { take, map } from 'rxjs/operators';

// zatim nechceme, aby nam do appky lezl kde kdo
// takze tady jsou povoleni uzivatele
const allowedUsers = [
  'jonas.hampeis@epptec.eu',
  'jonas.h331@gmail.com',
  'martin.hadacek@epptec.eu',
  'vaclav.kandrnal@epptec.eu',
  'karel.svec@epptec.eu',
  'tester@epptec.eu',
  'petr.lev@epptec.eu',
  'jan.silhavy@epptec.eu',
  'martin.fabry@epptec.eu',
  'ondrej.mach@epptec.eu',
  'pkovik@gmail.com',
  'petr.kovar@rififi.cz',
  'leos.rehacek@epptec.eu',
  'ondrej.brejla@epptec.eu',
];

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.authService.$user.pipe(
      take(1),
      map((user) => {
        const isAuth = !!user;
        if (isAuth) {
          if (allowedUsers.includes(user.email)) {
            return true;
          } else {
            this.router.navigate(['/police']);
          }
        } else {
          this.router.navigate(['/login']);
        }
      })
    );
  }
}
