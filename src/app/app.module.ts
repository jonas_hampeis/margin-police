import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { LandingComponent } from './examples/landing/landing.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RegisterComponent } from './auth/register/register.component';
import { SharedModule } from './shared/shared.module';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AuthService } from './services/auth.service';
import { registerLocaleData } from '@angular/common';
import localeCs from '@angular/common/locales/cs';
import { ProfileComponent } from './profile/profile.component';
import { PoliceModule } from './police/police.module';
import { MeterPointComponent } from './mocks/meter-point/meter-point.component';
import { LightModule } from './light/light.module';

registerLocaleData(localeCs);

const firebaseConfig = {
  apiKey: 'AIzaSyDvywYuUhWldjFZ5mWwI-iKhlKKpvSxLoo',
  authDomain: 'margin-police.firebaseapp.com',
  databaseURL: 'https://margin-police.firebaseio.com',
  projectId: 'margin-police',
  storageBucket: 'margin-police.appspot.com',
  messagingSenderId: '969060379527',
  appId: '1:969060379527:web:b6b2a51eb6f6dad14a4122',
  measurementId: 'G-98T49T3SV4',
};

// pri zapnuti appky nejdrive nacte info o uzivateli
export function loadAuthService(authService: AuthService): Function {
  return () => {
    return authService.getUserInfo();
  };
}

// aplikace vznikala pred navrhem datoveho modelu a tak obsahuje nazvy
// promennych a objektu, ktere ted nazyvame jinak
// budu se snazit je vsechny prejmenovat, ale pokud mi neco utece,
// zde je drobny slovnicek. format: jmeno v aplikaci = jmeno v datovem modelu
// supply point / consumption point = supply place
// meter = supply point
// review data = period summary
// deduction / entry / odecet = meter reading
// user contract tariff = user tariff
// accounting = billing

// pod constructory komponent jsem se snazil davat popisky, co komponenta zhruba dela

// vstup do aplikace je chranen AuthGuardem, ktery mimo jine pousti jen nektere maily
// pro pristup dalsiho uzivatele je potreba pridat mail do seznamu adres v AuthGuardu

// ve slozce police je aplikace tak, jak vznikala v ramci dema
// ve slozce light je odlehcena verze podle analyzy bez veskere funkcnosti

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LandingComponent,
    ProfileComponent,
    MeterPointComponent,
  ],
  imports: [
    SharedModule,
    PoliceModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    // pro textaci a vicejazycnost pouzivame TranslateModule
    // texty jsou ulozeny v assets/i18n
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    LightModule,
  ],
  exports: [],
  providers: [
    AuthService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadAuthService,
      deps: [AuthService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
