# PowerHawk

Postaveno na Angularu s Now UI Kitem

Soft analýza: https://docs.google.com/document/d/184DdOyV4_qQV9rBGWSkg22re_GqXBlAvMEIrspUjds8/edit?usp=sharing

Business analýza: https://docs.google.com/document/d/16E9vpQqNdYqoRmYNZwF8t4I7HqHCOnUERrpP5ymabIg/edit?usp=sharing

Analýza Google Sheets (jsou tam data pro firestore): https://docs.google.com/spreadsheets/d/1r9GKZtdwwz0wCaFtky0xAy6XmNqQe0tLPldCVuu-xTI/edit?usp=sharing

Dokumentace (relevantní je asi jenom nákres v aplikaci použitého datového modelu): https://docs.google.com/document/d/1Vop7tDC-oiU-NTVem2Tu_4bkTSa1QjZID6Z7OYqWUzY/edit?usp=sharing

Datový model (budoucí): https://drive.google.com/file/d/1QrBmwLj1WQkaoJtF96yfaUOzKyh7QZjD/view?usp=sharing

Pro přihlášení do aplikace je potřeba přidat svojí mailovou adresu do AuthGuardu v `margin-police\src\app\auth\auth.guard.ts`

Aplikace se dělí do dvou hlavních modulů: PoliceModule a LightModule. PoliceModule je aplikace tak, jak vznikala v rámci dema, LightModule je v podstatě jen html verze aplikace, kterou je se možné proklikat, nemá však žádnou funkcionalitu. LightModule existuje pro účely testování nápádů, které vznikly během analýzy a pro ukázku v rámci analýzy.

Aplikace se dříve nazývala MarginPolice, z čehož vznikl název hlavního modulu. V rámci analýzy jsme přejmenovali více věcí, pro přehlednost je v `app.module.ts` umístěn drobný slovníček, který snad vyjasní některé zmatky, které přejmenováním nastaly.
